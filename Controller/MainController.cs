﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Entities;
using ximc;

/*
 * Order:
 * Enumerate Devices into IntPtr                  
 * Device Count into int                          
 * IF device count positive, select which to open 
 *      Device name into string
 *      Close all devices
 *      Open device into int - deviceID
 * Get stats for UI and poll for errors constantly
 * */

/*
 * NOTE:
 * Not sure if making actuatorInContext public (property) is a good idea
 * It's easier this way because in View you can directly access parameters for the UI
 * But becase of this I can (if I really want to) use the methods from ActuatorController in View, 
 * which I do not want to do because View should have minimal to no logic
 */

/* NOTE
 * When we open a device, should we automatically close all other and then open the selected one
 * or is it a good idea to close all devices in the back-end when we call OpenDevice(string deviceName)?
 * Is there any case in which we want to call OpenDevice(string deviceName) without closing all other beforehand?
 */

namespace Controller
{
    public class MainController
    {
        Actuators actuators;
        ActuatorController actuatorInContext;
        ActuatorErrorHandler actuatorErrorHandler;
        LogController logController;
        BackgroundWorker bgWorkerErrorPolling;
        BackgroundWorker bgWorkerPollForActuators;
        List<Notification> notifList;
        bool isFirstTimeLaunch;

        static readonly object _lockerEnum = new object();
        static readonly object _lockerGetCount = new object();

public void Metoda()
{
    // Aplicare lacatuire
    Monitor.Enter(_lockerEnum);

    Console.WriteLine("Cod lacatuit");

    // Eliberare lacat
    Monitor.Exit(_lockerEnum); 
}

        public MainController()
        {
            Actuators = new Actuators();
            ActuatorInContext = new ActuatorController();
            actuatorErrorHandler = new ActuatorErrorHandler(this);
            logController = new LogController();
            notifList = new List<Notification>();
            isFirstTimeLaunch = true;

            InitActuators();
            InitBgWorkers();
        }

        void bgWorkerErrorPollingDoWork(object obj, DoWorkEventArgs e)
        {
            while (true)
            {
                bgWorkerErrorPolling.ReportProgress(0);
                Thread.Sleep(50);
            }
        }

        /*TO DO - turned out not to work, to delete or rethink*/
        void bgWorkerPollForActuatorsDoWork(object obj, DoWorkEventArgs e)
        {
            while (true)
            {
                bgWorkerPollForActuators.ReportProgress(0);
                Thread.Sleep(100);
            }
        }

        // TO DELETE
        void bgWorkerErrorPollingProgressChanged(object obj, ProgressChangedEventArgs e)
        {
            if (ActuatorInContext.DeviceID != -1)
                actuatorErrorHandler.CheckForActuatorErrors(ActuatorInContext.GetStatus(ActuatorInContext.DeviceID));

            // If device is -1, meaning that actuator got disconncted
            else
            {
                // Try to get another actuator in context, the first one found in the list
                if (Actuators.Count > 0)
                    ChangeContext(Actuators.List[0].DeviceID);

                // Await actuator connection
                else
                { }
            }
        }

        // TO DO - CHANGE
        void bgWorkerPollForActuatorsChanged(object obj, ProgressChangedEventArgs e)
        {
            Actuators.EnumerateActuators();

            // If new actuator detected
            if (Actuators.Count < Actuators.GetDeviceCountNoOverwrite())
            {
                //gWorkerErrorPolling.CancelAsync();

                Actuators = new Actuators();
                Actuators.InitializeActuators();

                API.open_device(API.get_device_name(API.enumerate_devices(0, ""), 0));

                Actuators.EnumerateActuators();

                // Update new device count and get list index value
                int newIndex = Actuators.GetDeviceCount() - 1;

                // Add to list new actuator
                Actuators.List.Add(new ActuatorController(newIndex));

                // Get new actuator's name
                Actuators.List[newIndex].DeviceName = Actuators.List[newIndex].GetDeviceName(Actuators.ActuatorEnumeration, newIndex);

                // Get new actuator's ID & open
                Actuators.List[newIndex].DeviceID = Actuators.List[newIndex].OpenDevice(Actuators.List[newIndex].DeviceName);

                // Get all of its info
                Actuators.List[newIndex].GetActuatorInformation(Actuators.List[newIndex].DeviceID);

                // Close new actuator
                Actuators.List[newIndex].CloseDevice(ref newIndex);

                //actuators.ProcessNewActuator();

                //bgWorkerErrorPolling.RunWorkerAsync();
                // Update UI somehow
            }
        }

        public void ChangeContext(int listIndex = 0)
        {
            // Engage thread lock
            Monitor.Enter(_lockerEnum);

            if (isFirstTimeLaunch == true)
            {
                Actuators.List[listIndex].OpenDevice(Actuators.List[listIndex].DeviceName);
                ActuatorInContext = Actuators.List[listIndex];
                isFirstTimeLaunch = false;
                logController.LogActuatorInfo(Enums.ActuatorLog.NewActuatorInContext, ActuatorInContext);
            }
            else

            // Only change the context if the new device is different from the one already opened
            if (Actuators.List[listIndex].DeviceID != ActuatorInContext.DeviceID)
            {
                CloseAllActuators();

                Actuators.List[listIndex].OpenDevice(Actuators.List[listIndex].DeviceName);

                // Set the new actuator in context (which is currently in use by the controller)
                ActuatorInContext = Actuators.List[listIndex];
                logController.LogActuatorInfo(Enums.ActuatorLog.NewActuatorInContext, ActuatorInContext);
            }

            // Disengage thread lock
            Monitor.Exit(_lockerEnum);
        }

        public void ChangeContextByAxis(Enums.Axis axis)
        {
            // Engage thread lock
            Monitor.Enter(_lockerEnum);

            int listIndex = -1;

            CloseAllActuators();

            foreach (ActuatorController actuator in Actuators.List)
                if (actuator.Axis == axis)
                    listIndex = actuator.ListIndex;

            Actuators.List[listIndex].OpenDevice(Actuators.List[listIndex].DeviceName);

            ActuatorInContext = Actuators.List[listIndex];

            // Disengage thread lock
            Monitor.Exit(_lockerEnum);
        }

        public void CloseAllActuators()
        {
            Random rand = new Random();

            for (int i = 0; i < Actuators.Count; i++)
            {
                int refID = Actuators.List[i].DeviceID;

                // Very dirty
                Actuators.List[i].DeviceID = rand.Next(-999999999, 999999999);

                Actuators.List[i].CloseDevice(ref refID);
            }
        }

        public void ActuatorCloseDevice(int deviceID)
        { 
            ActuatorInContext.CloseDevice(ref deviceID);
        }

        public List<int> ActuatorCalibrateMovement(int deviceID)
        {
            return ActuatorInContext.CalibrateMovement(deviceID);
        }

        public void MoveHome(int deviceID)
        {
            if (ActuatorInContext.MoveToHomePosition(deviceID) == Result.ok)
            {
                //Task asyncTask = Task.Run(() =>
                //{
                //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                //        logController.LogActuatorInfo(Enums.ActuatorLog.MoveHome, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                //});
            }
        }

        public void ActuatorMoveHome(int deviceID)
        {
            if (ActuatorInContext.MoveToHomePosition(deviceID) == Result.ok)
                if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                {
                    //Task asyncTask = Task.Run(() =>
                    //{
                    //    logController.LogActuatorInfo(Enums.ActuatorLog.MoveHome, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                    //});
                }
        }

        // MoveContinuouslyLeft is unsafe! Using MoveToPosition instead
        public void ActuatorMoveContinuouslyLeft(int deviceID, int minEdgeSoftware)
        {
            if (ActuatorInContext.MoveToPosition(deviceID, minEdgeSoftware, 0) == Result.ok)
            {
                //Task asyncTask = Task.Run(() =>
                //{
                //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                //        logController.LogActuatorInfo(Enums.ActuatorLog.MoveToPosition, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                //});
            }
        }

        public void ActuatorMoveToPosition(int deviceID, int positionSteps, int positionMicrosteps)
        {
            if (ActuatorInContext.MoveToPosition(deviceID, positionSteps, positionMicrosteps) == Result.ok)
            {
                //Task asyncTask = Task.Run(() =>
                //{
                //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                //        logController.LogActuatorInfo(Enums.ActuatorLog.MoveToPosition, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                //});
            }
        }

        public void ActuatorMoveRelatively(int deviceID, int deltaSteps, int deltaMicrosteps)
        {
            if (ActuatorInContext.MoveRelatively(deviceID, deltaSteps, deltaMicrosteps) == Result.ok)
            {
                //Task asyncTask = Task.Run(() =>
                //{
                //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                //        logController.LogActuatorInfo(Enums.ActuatorLog.MoveRelatively, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                //});
            }
        }

        public void ActuatorPowerOff(int deviceID)
        {
            ActuatorInContext.PowerOff(deviceID);
        }

        // MoveContinuouslyRight is unsafe! Using MoveToPosition instead
        public void ActuatorMoveContinuouslyRight(int deviceID, int maxEdgeSoftware)
        {
            if (ActuatorInContext.MoveToPosition(deviceID, maxEdgeSoftware, 0) == Result.ok)
            {
                //Task asyncTask = Task.Run(() =>
                //{
                //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                //        logController.LogActuatorInfo(Enums.ActuatorLog.MoveToPosition, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                //});
            }
        }


        public void ActuatorSoftStop(int deviceID)
        {
            if (ActuatorInContext.GetStatus(ActuatorInContext.DeviceID).CurSpeed != 0 || ActuatorInContext.GetStatus(ActuatorInContext.DeviceID).uCurSpeed != 0)
                if (ActuatorInContext.SoftStop(deviceID) == Result.ok)
                {
                    //Task asyncTask = Task.Run(() =>
                    //{
                    //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                    //        logController.LogActuatorInfo(Enums.ActuatorLog.SoftStop, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                    //});
                }
        }

        public void ActuatorHardStop(int deviceID)
        {
            if (ActuatorInContext.GetStatus(ActuatorInContext.DeviceID).CurSpeed != 0 || ActuatorInContext.GetStatus(ActuatorInContext.DeviceID).uCurSpeed != 0)
                if (ActuatorInContext.HardStop(deviceID) == Result.ok)
                {
                    //Task asyncTask = Task.Run(() =>
                    //{
                    //    if (ActuatorInContext.WaitForStopWhile(deviceID, 0) == Result.ok)
                    //        logController.LogActuatorInfo(Enums.ActuatorLog.HardStop, ActuatorInContext, Enums.ActuatorLogTiming.Finish);
                    //});
                }
        }

        // TO DELETE
        public bool HasActuatorCountReduced()
        {
            Actuators.EnumerateActuators(true);

            if (Actuators.Count > Actuators.GetDeviceCountNoOverwrite())
                return true;
            else return false;
        }

        // TO DELETE
        public int GetNewActuatorCount()
        {
            return Actuators.GetDeviceCount();
        }

        // TO DELETE
        public void DisconnectActuator()
        {
            Actuators.RemoveActuatorFromList();
            ActuatorInContext.DeviceID = -1;
        }

        private void InitActuators()
        {
            Actuators.InitializeActuators();

            if (Actuators.Count > 0)
                ChangeContext();
            else
                logController.LogMultipleActuatorsError(Enums.MultipleActuatorsLog.NoDevicesFound);
        }

        private void InitBgWorkers()
        {
            bgWorkerErrorPolling = new BackgroundWorker();
            bgWorkerPollForActuators = new BackgroundWorker();

            bgWorkerErrorPolling.DoWork += new DoWorkEventHandler(bgWorkerErrorPollingDoWork);
            bgWorkerErrorPolling.ProgressChanged += new ProgressChangedEventHandler(bgWorkerErrorPollingProgressChanged);
            bgWorkerErrorPolling.WorkerSupportsCancellation = true;
            bgWorkerErrorPolling.WorkerReportsProgress = true;

            bgWorkerPollForActuators.DoWork += new DoWorkEventHandler(bgWorkerPollForActuatorsDoWork);
            bgWorkerPollForActuators.ProgressChanged += new ProgressChangedEventHandler(bgWorkerPollForActuatorsChanged);
            bgWorkerPollForActuators.WorkerSupportsCancellation = true;
            bgWorkerPollForActuators.WorkerReportsProgress = true;

            //bgWorkerErrorPolling.RunWorkerAsync();
            //bgWorkerPollForActuators.RunWorkerAsync();
        }

        public ActuatorController ActuatorInContext { get => actuatorInContext; set => actuatorInContext = value; }
        public Actuators Actuators { get => actuators; set => actuators = value; }
        public List<Notification> NotifList { get => notifList; set => notifList = value; }
    }
}
