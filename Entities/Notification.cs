﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Notification
    {
        private Enums.NotificationType notifType;
        private string notifString;
        private bool wasDisplayed;
        static List<Notification> notifList;

        static Notification()
        {
            notifList = new List<Notification>();
        }

        public Notification()
        {
            this.NotifType = new Enums.NotificationType();
            this.NotifString = "";
            this.WasDisplayed = false;
            notifList.Add(this);
        }

        public Notification(Enums.NotificationType notifType, string notifString)
        {
            this.notifType = notifType;
            this.notifString = notifString;
            this.wasDisplayed = false;
        }

        public Enums.NotificationType NotifType { get => notifType; set => notifType = value; }
        public string NotifString { get => notifString; set => notifString = value; }
        public bool WasDisplayed { get => wasDisplayed; set => wasDisplayed = value; }
        public static List<Notification> NotifList { get => notifList; set => notifList = value; }
    }
}
