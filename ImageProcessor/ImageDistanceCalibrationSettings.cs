﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessor
{
    public class ImageDistanceCalibrationSettings
    {
        private bool isEnabled;
        private int knownDistanceMilimeters;
        private int distancePixels;
        private float milimetersPerPixel;
        private float translatedDistanceMilimeters;
        private float stepsPerPixel;

        public ImageDistanceCalibrationSettings()
        {
            this.IsEnabled = false;
            this.KnownDistanceMilimeters = 0;
            this.DistancePixels = 0;
            this.MilimetersPerPixel = 0f;
            this.TranslatedDistanceMilimeters = 0f;
            this.StepsPerPixel = 0f;
        }

        public ImageDistanceCalibrationSettings(bool isEnabled, int knownDistanceMilimeters, int distancePixels, float milimetersPerPixel, float translatedDistanceMilimeters, float stepsPerPixel)
        {
            this.isEnabled = isEnabled;
            this.knownDistanceMilimeters = knownDistanceMilimeters;
            this.distancePixels = distancePixels;
            this.milimetersPerPixel = milimetersPerPixel;
            this.translatedDistanceMilimeters = translatedDistanceMilimeters;
            this.stepsPerPixel = stepsPerPixel;
        }

        public void CalculateMilimetersPerPixel(int knownDistMM, int distPx)
        {
            this.KnownDistanceMilimeters = knownDistMM;
            this.DistancePixels = distPx;

            // Calcul milimetri per pixel
            this.MilimetersPerPixel = (float)KnownDistanceMilimeters / (float)DistancePixels;
        }

        public void CalculateTranslatedDistanceMilimeters()
        {
            this.TranslatedDistanceMilimeters = (float)MilimetersPerPixel * (float)DistancePixels;
        }

        public void CalculateStepsPerPixel()
        {
            // 800 because 1mm = 800 steps
            this.StepsPerPixel = (float)MilimetersPerPixel * 800;
        }

        public bool IsEnabled { get => isEnabled; set => isEnabled = value; }
        public int KnownDistanceMilimeters { get => knownDistanceMilimeters; set => knownDistanceMilimeters = value; }
        public int DistancePixels { get => distancePixels; set => distancePixels = value; }
        public float MilimetersPerPixel { get => milimetersPerPixel; set => milimetersPerPixel = value; }
        public float TranslatedDistanceMilimeters { get => translatedDistanceMilimeters; set => translatedDistanceMilimeters = value; }
        public float StepsPerPixel { get => stepsPerPixel; set => stepsPerPixel = value; }
    }
}
