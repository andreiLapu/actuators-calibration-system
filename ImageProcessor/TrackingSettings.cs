﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessor
{
    public class TrackingSettings
    {
        private bool isEnabled;
        private bool justChangedAxis;
        private int frameCount;
        private int frameSkipCount;
        private int pixelTolerance;
        private System.Drawing.Point lastestTrackedStepsPoint;
        private System.Drawing.Point lastestTrackedPixelPoint;
        private System.Drawing.Point lastestCenterPixelPoint;

        private int defaultPixelTolerance = 4;
        private int defaultFrameSkipCount = 4;

        // TO DO - parametrizare pe UI
        public TrackingSettings()
        {
            this.isEnabled = false;
            this.JustChangedAxis = false;
            this.frameCount = 0;
            this.frameSkipCount = defaultPixelTolerance;
            this.PixelTolerance = defaultFrameSkipCount;
            this.lastestTrackedStepsPoint = new Point();
            this.lastestTrackedPixelPoint = new Point();
            this.lastestCenterPixelPoint = new Point();
        }

        public TrackingSettings(bool isEnabled, int frameCount, int frameSkipCount, Point lastestTrackedStepsPoint, Point lastestTrackedPixelPoint, Point lastestCenterPixelPoint)
        {
            this.isEnabled = isEnabled;
            this.frameCount = frameCount;
            this.frameSkipCount = frameSkipCount;
            this.lastestTrackedStepsPoint = lastestTrackedStepsPoint;
            this.lastestTrackedPixelPoint = lastestTrackedPixelPoint;
            this.lastestCenterPixelPoint = lastestCenterPixelPoint;
        }

        public void SetDefaultValues(int defPixelTolerance, int defFrameSkipCount)
        {
            defaultPixelTolerance = defPixelTolerance;
            defaultFrameSkipCount = defFrameSkipCount + 1;
        }

        public int ResetPixelTolerance()
        {
            return PixelTolerance = defaultPixelTolerance;
        }

        public int ResetFrameSkipCount()
        {
            return FrameSkipCount = defaultFrameSkipCount;
        }

        public bool IsEnabled { get => isEnabled; set => isEnabled = value; }
        public bool JustChangedAxis { get => justChangedAxis; set => justChangedAxis = value; }
        public int FrameCount { get => frameCount; set => frameCount = value; }
        public int FrameSkipCount { get => frameSkipCount; set => frameSkipCount = value; }
        public Point LastestTrackedStepsPoint { get => lastestTrackedStepsPoint; set => lastestTrackedStepsPoint = value; }
        public Point LastestTrackedPixelPoint { get => lastestTrackedPixelPoint; set => lastestTrackedPixelPoint = value; }
        public Point LastestCenterPixelPoint { get => lastestCenterPixelPoint; set => lastestCenterPixelPoint = value; }
        public int PixelTolerance { get => pixelTolerance; set => pixelTolerance = value; }
    }
}
