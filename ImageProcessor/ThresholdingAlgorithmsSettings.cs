﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controller;
using Entities;

namespace ImageProcessor
{
    public class ThresholdingAlgorithmsSettings
    {
        private int _HIST_HEIGHT = 300;
        private int _HIST_BINS_COUNT = 240;
        private int _THRESH_DIVIDE_VAL = 100;
        private int _THRESH_OFFSET_AMOUNT = 0;
        private int _BLUR_KERNEL_SIZE = 7;
        private int _MEDIAN_KERNEL_SIZE = 7;

        private bool isEnabled;
        private bool isInverted;
        private Enums.ImgProcAlgorithm algorithm;
        private int staticThresholdValue;

        private int precision;
        private int quality;
        private int brightness;
        private List<Enums.Axis> connectedAxisList;

        private int defaultPrecisionSliderValue;
        private int defaultQualitySliderValue;
        private int defaultBrightnessSliderValue;
        private int defaultPrecision;

        private static int defaultStaticThresholdValue = 128;

        public ThresholdingAlgorithmsSettings()
        {
            this._HIST_HEIGHT = 300;
            this._HIST_BINS_COUNT = 240;
            this._THRESH_DIVIDE_VAL = 100;
            this._THRESH_OFFSET_AMOUNT = 0;
            this._BLUR_KERNEL_SIZE = 7;
            this._MEDIAN_KERNEL_SIZE = 7;

            this.IsEnabled = false;
            this.IsInverted = false;
            this.Algorithm = Enums.ImgProcAlgorithm.None;
            this.StaticThresholdValue = 128;

            this.precision = 10;
            this.quality = 1;
            this.brightness = 0;
            this.ConnectedAxisList = new List<Enums.Axis>();
        }

        public int CalculatePrecision(double sliderValue)
        {
            return (int)Math.Pow(10, sliderValue) / 10;
        }

        public void SetDefaultValues(int defPrecisionSliderValue, int defQualitySliderValue, int defBrightnessSliderValue)
        {
            this.defaultPrecisionSliderValue = defPrecisionSliderValue;
            this.defaultQualitySliderValue = defQualitySliderValue;
            this.defaultBrightnessSliderValue = defBrightnessSliderValue;
            this.defaultPrecision = Precision = CalculatePrecision(defPrecisionSliderValue);

            this.Precision = CalculatePrecision(defPrecisionSliderValue);
            this.Quality = defQualitySliderValue;
            this.Brightness = defBrightnessSliderValue;
        }

        public int ResetPrecision()
        {
            return Precision = defaultPrecision;
        }

        public int ResetQuality()
        {
            return Quality = defaultQualitySliderValue;
        }

        public int ResetBrightness()
        {
            return Brightness = defaultBrightnessSliderValue;
        }

        public int ResetStaticThresholdValue()
        {
            return this.StaticThresholdValue = defaultStaticThresholdValue;
        }

        public void SetConnectedAxis(Actuators actuators)
        {
            ConnectedAxisList = new List<Enums.Axis>();

            for (int i = 0; i < Actuators.List.Count; i++)
                ConnectedAxisList.Add(Actuators.List[i].Axis);
        }

        public bool IsEnabled { get => isEnabled; set => isEnabled = value; }
        public Enums.ImgProcAlgorithm Algorithm { get => algorithm; set => algorithm = value; }
        public int StaticThresholdValue { get => staticThresholdValue; set => staticThresholdValue = value; }
        public bool IsInverted { get => isInverted; set => isInverted = value; }

        public int Precision { get => precision; set => precision = value; }
        public int Quality { get => quality; set => quality = value; }
        public int Brightness { get => brightness; set => brightness = value; }
        public List<Enums.Axis> ConnectedAxisList { get => connectedAxisList; set => connectedAxisList = value; }
        public int DefaultPrecisionSliderValue { get => defaultPrecisionSliderValue; set => defaultPrecisionSliderValue = value; }
        public int DefaultQualitySliderValue { get => defaultQualitySliderValue; set => defaultQualitySliderValue = value; }
        public int DefaultBrightnessSliderValue { get => defaultBrightnessSliderValue; set => defaultBrightnessSliderValue = value; }

        public int HIST_HEIGHT { get => _HIST_HEIGHT; set => _HIST_HEIGHT = value; }
        public int HIST_BINS_COUNT { get => _HIST_BINS_COUNT; set => _HIST_BINS_COUNT = value; }
        public int THRESH_DIVIDE_VAL { get => _THRESH_DIVIDE_VAL; set => _THRESH_DIVIDE_VAL = value; }
        public int THRESH_OFFSET_AMOUNT { get => _THRESH_OFFSET_AMOUNT; set => _THRESH_OFFSET_AMOUNT = value; }
        public int BLUR_KERNEL_SIZE { get => _BLUR_KERNEL_SIZE; set => _BLUR_KERNEL_SIZE = value; }
        public int MEDIAN_KERNEL_SIZE { get => _MEDIAN_KERNEL_SIZE; set => _MEDIAN_KERNEL_SIZE = value; }
    }
}
