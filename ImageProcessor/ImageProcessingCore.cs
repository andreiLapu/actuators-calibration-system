﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using Entities;
using System.Drawing;
using System.Threading;
using System.Diagnostics;

// Image processesing core. Every algorithm or setting can be accesed through here
namespace ImageProcessor
{
    public class ImageProcessingCore
    {
        private static VideoCapture capture;

        ThresholdingAlgorithms threshAlgorithms;
        ThresholdingAlgorithmsSettings taSettings;
        ImageDistanceCalibration idc;
        ImageDistanceCalibrationSettings idcSettings;
        TrackingSettings trackingSettings;
        VideoFeedSettings videoFeedSettings;
        static List<float> frameAcqTimeList = new List<float>();


        public ImageProcessingCore()
        {
            this.threshAlgorithms = new ThresholdingAlgorithms(this);
            this.TASettings = new ThresholdingAlgorithmsSettings();
            this.idc = new ImageDistanceCalibration(this);
            this.IDCSettings = new ImageDistanceCalibrationSettings();
            this.TrackingSettings = new TrackingSettings();
            this.VideoFeedSettings = new VideoFeedSettings();
        }

        public void StartCapture()
        {
            int fps = 60;

            capture = new VideoCapture(1);
            SetFramesPerSecond(fps);
        }

        public void StartCaptureWithResolution(int width, int height)
        {
            capture = new VideoCapture(1);

            if (capture != null && capture.IsDisposed == false)
            {
                capture.Set(CaptureProperty.FrameHeight, height);
                capture.Set(CaptureProperty.FrameWidth, width);
            }
        }

        public void StopCapture()
        {
            if (capture != null && capture.IsDisposed == false)
            {
                capture.Release();
                capture.Dispose();
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Video feed was stopped"));
            }
        }

        public void SetFramesPerSecond(int value)
        {
            if (capture != null && capture.IsDisposed == false)
                capture.Set(CaptureProperty.Fps, value);
        }

        public (int, int) GetResolution()
        {
            if (capture != null && capture.IsDisposed == false)
            {
                double width = capture.Get(CaptureProperty.FrameWidth);
                double height = capture.Get(CaptureProperty.FrameHeight);

                return ((int)width, (int)height);
            }
            else return (-1, -1);
        }

        public (Bitmap, System.Drawing.Point) StartThresholdingAlgorithms(System.Drawing.Point actuatorPositionPixels)
        {
            return threshAlgorithms.Run(actuatorPositionPixels);
        }

        public (Bitmap, int) StartImageDistanceCalibration()
        {
            return idc.Run();
        }

        public void SetImageProcessingParameters()
        {
            TASettings.THRESH_DIVIDE_VAL = TASettings.Precision;
            TASettings.BLUR_KERNEL_SIZE = TASettings.MEDIAN_KERNEL_SIZE = TASettings.Quality;
        }

        // TO DO - check functionality
        public void IsVideoCaptureDisposed()
        {
            if (Capture.IsDisposed)
                StartCapture();
        }

        public (Mat, bool) TryReadVideoCaptureFrame()
        {
            Mat frame = new Mat();

            /* ACQ FRAME*/
            //Stopwatch frameAcqTime = new Stopwatch();
            //frameAcqTime.Start();

            // Read frame from capturing device
            Capture.Read(frame);

            //frameAcqTime.Stop();
            //frameAcqTimeList.Add(frameAcqTime.ElapsedMilliseconds);
            //Console.WriteLine("Timp mediu achizitie cadru webcam: " + frameAcqTimeList.Average() + " now = " + frameAcqTime.ElapsedMilliseconds);
            /* */



            // If size is invalid => frame is invalid
            if (frame.Height * frame.Width == 0)
                return (new Mat(), false);

            else return (frame, true);
        }

        public Mat CropToROI(Mat img)
        {
            Rect ROI;

            //Cv2.ImShow("Imagine originala", img);

            // If images are the same height and width, no crop is necessary
            if (img.Width == img.Height)
                return img;

            // If image width is larger than image height
            else if (img.Width > img.Height)
                ROI = new Rect((img.Width - img.Height) / 2, 0, img.Height, img.Height);

            // If image height is larger than image width
            else ROI = new Rect((img.Height - img.Width) / 2, 0, img.Width, img.Width);

            // Create empty image with specified size
            Mat emptyCrop = new Mat(img, ROI);

            // Fill new image with data
            Mat newImg = new Mat();
            emptyCrop.CopyTo(newImg);

            //Cv2.ImShow("Imagine decupata", newImg);

            return newImg;
        }

        public Mat CropToROI2(Mat img)
        {
            Rect ROI;

            //Cv2.ImShow("Imagine originala", img);

            // If images are the same height and width, no crop is necessary
            if (img.Width == img.Height)
                return img;

            // If image width is larger than image height
            else if (img.Width > img.Height)
                ROI = new Rect(448, 28, 1024, 1024);

            // If image height is larger than image width
            else ROI = new Rect((img.Height - img.Width) / 2, 0, img.Width, img.Width);

            // Create empty image with specified size
            Mat emptyCrop = new Mat(img, ROI);

            // Fill new image with data
            Mat newImg = new Mat();
            emptyCrop.CopyTo(newImg);

            //Cv2.ImShow("Imagine decupata", newImg);

            return newImg;
        }

        public Mat ConvertToGrayscale(Mat img)
        {
            Mat imgGrayscale = new Mat();
            Cv2.CvtColor(img, imgGrayscale, ColorConversionCodes.BGR2GRAY);

            //Cv2.ImShow("Imagine in tonuri de gri", imgGrayscale);

            return imgGrayscale;
        }

        public Mat SetImageBrightness(Mat imgGrayscale)
        {
            imgGrayscale.ConvertTo(imgGrayscale, MatType.CV_8UC1, 1, TASettings.Brightness);

            //Cv2.ImShow("Imagine cu luminozitate +40", imgGrayscale);

            return imgGrayscale;
        }

        public Mat SimpleImageBlur(Mat img)
        {
            Mat imgBlurred = new Mat();
            Cv2.Blur(img, imgBlurred, new OpenCvSharp.Size(TASettings.BLUR_KERNEL_SIZE, TASettings.BLUR_KERNEL_SIZE), new OpenCvSharp.Point(-1, -1));

            //Cv2.ImShow("Imagine cu filtru de mediere cu kernel de 9", imgBlurred);

            return imgBlurred;
        }

        public Mat CalculateHistogram(Mat img)
        {
            Mat histogram = new Mat();
            Cv2.CalcHist(new Mat[] { img }, new int[] { 0 }, new Mat(), histogram, 1, new int[] { TASettings.HIST_BINS_COUNT }, new Rangef[] { new Rangef(0, TASettings.HIST_BINS_COUNT) }, true, false);

            // Create a window and display historgam
            //ShowHistogram(Mat.Zeros(TASettings.HIST_HEIGHT, TASettings.HIST_BINS_COUNT, MatType.CV_8UC1), histogram, TASettings.HIST_HEIGHT, TASettings.HIST_BINS_COUNT);

            return histogram;
        }

        // TO DO - fail for IDC high brightness
        public void ShowHistogram(Mat imgHistogram, Mat histogram, int histogramHeight, int binCount)
        {
            double maxHistValue = FindMaxHistogramValue(histogram);

            for (int binIndex = 0; binIndex < binCount; binIndex++)
                Cv2.Line(imgHistogram,
                         new OpenCvSharp.Point(binIndex, histogramHeight - Convert.ToInt32(histogram.At<float>(binIndex) * histogramHeight / maxHistValue)),
                         new OpenCvSharp.Point(binIndex, histogramHeight),
                         Scalar.All(255));

            Cv2.ImShow("Hitsograma", imgHistogram);
        }

        public double FindMaxHistogramValue(Mat hisogram)
        {
            Cv2.MinMaxLoc(hisogram, out double minValue, out double maxValue);

            return maxValue;
        }

        public int FindMaxHistogramIndex(Mat histogram, double maxHistValue)
        {
            Cv2.MinMaxLoc(histogram, out double minValue, out double maxValue, out OpenCvSharp.Point minHistIndex, out OpenCvSharp.Point maxHistIndex);

            return maxHistIndex.Y;
        }

        public int FindHistogramDividedIndex(Mat histogram, Enums.HistogramCalculation histCalculation, double maxHistValue, int maxHistIndex)
        {
            if (histCalculation == Enums.HistogramCalculation.Normal)
            {
                // Divided threshold value will me maximum threshold value divided by predefined value (THRESH_DIVIDE_VAL)
                for (int dividedValIdx = maxHistIndex; dividedValIdx < TASettings.HIST_BINS_COUNT; dividedValIdx++)
                    if (histogram.At<float>(dividedValIdx) <= ((float)maxHistValue / TASettings.THRESH_DIVIDE_VAL))
                        return dividedValIdx;

                // Return maximum histogram value if no other value was found
                return (int)maxHistValue;
            }
            else
            {
                for (int dividedValIdx = maxHistIndex; dividedValIdx >= 0; dividedValIdx--)
                    if (histogram.At<float>(dividedValIdx) <= ((float)maxHistValue / TASettings.THRESH_DIVIDE_VAL))
                        return dividedValIdx;

                // Return maximum histogram value if no other value was found
                return (int)maxHistValue;
            }
        }

        public int GetThresholdValue(int dividedValIndex)
        {
            return dividedValIndex + TASettings.THRESH_OFFSET_AMOUNT;
        }

        public Mat ThresholdFilter(Mat img, int thresholdValue, ThresholdTypes threshType)
        {
            Mat imgThreshold = new Mat();
            Cv2.Threshold(img, imgThreshold, thresholdValue, 255, threshType);

            //Cv2.ImShow("Imagine binarizata cu T = 128", imgThreshold);

            return imgThreshold;
        }

        public Mat MedianFilter(Mat img)
        {
            Mat imgMedian = new Mat();

            for (int i = 1; i < TASettings.MEDIAN_KERNEL_SIZE; i += 2)
                Cv2.MedianBlur(img, imgMedian, i);

            //Cv2.ImShow("Imagine cu filtru median cu kernel de 9", imgMedian);

            return imgMedian;
        }

        public Mat[] FindAllContours(Mat img)
        {
            Cv2.FindContours(img, out Mat[] allContours, new Mat(), RetrievalModes.External, ContourApproximationModes.ApproxSimple);

            return allContours;
        }

        // TO DO
        // Simplify?
        public Mat FindLargestContourArea(Mat img, Mat[] allContours)
        {
            double maxAreaValue = -1;
            int maxAreaIndex = -1;

            for (int i = 0; i < allContours.Count(); i++)
            {
                // Compute contour area
                double newArea = Cv2.ContourArea(allContours[i]);

                if (newArea >= maxAreaValue)
                {
                    maxAreaValue = newArea;
                    maxAreaIndex = i;
                }
            }

            //Mat imgComposed = new Mat();
            //Cv2.CvtColor(img, imgComposed, ColorConversionCodes.GRAY2BGR);
            // Cv2.DrawContours(imgComposed, new Mat[] { allContours[maxAreaIndex] }, -1, Scalar.FromRgb(255, 128, 255), 2);
            // Cv2.ImShow("Conturul cu cea mai mare arie", imgComposed);

            return allContours[maxAreaIndex];
        }

        public Mat[] FindLargestTwoContours(Mat img, Mat[] allContours)
        {
            if (allContours.Length > 1)
            {
                for (int i = 0; i < allContours.Length - 1; i++)
                    for (int j = i + 1; j < allContours.Length; j++)
                        if ((allContours[i].Height * allContours[i].Width) < (allContours[j].Height * allContours[j].Width))
                        {
                            Mat temp = allContours[i];
                            allContours[i] = allContours[j];
                            allContours[j] = temp;
                        }

                return new Mat[] { allContours[0], allContours[1] };
            }
            else
                return new Mat[] { };
        }

        public OpenCvSharp.Point FindContourCenter(Mat img, Mat largestContourArea)
        {
            Moments mom = new Moments(largestContourArea);

            return new OpenCvSharp.Point(mom.M10 / mom.M00, mom.M01 / mom.M00);
        }

        public Mat InvertImage(Mat img)
        {
            Mat invertedImg = new Mat();
            Cv2.BitwiseNot(img, invertedImg);

            //Cv2.ImShow("Imagine negativata", invertedImg);

            return invertedImg;
        }

        public double CalculateEuclidianDistance(List<OpenCvSharp.Point> pointsList)
        {
            return Math.Sqrt(Math.Pow(pointsList[0].X - pointsList[1].X, 2) + Math.Pow(pointsList[0].Y - pointsList[1].Y, 2));
        }

        public Mat ComposeImageDTC(Mat img, Mat largestContourArea, OpenCvSharp.Point contourCenter, System.Drawing.Point actuatorPositionPixels)
        {
            Mat imgComposed = new Mat();

            // Convert to RGB, draw largest contour, draw largest contour center, put coordinates text, draw actuators position
            Cv2.CvtColor(img, imgComposed, ColorConversionCodes.GRAY2BGR);
            Cv2.DrawContours(imgComposed, new Mat[] { largestContourArea }, -1, Scalar.FromRgb(0, 128, 255), 2);
            Cv2.Circle(imgComposed, contourCenter, 2, Scalar.FromRgb(250, 50, 50), 5);
            Cv2.PutText(imgComposed, contourCenter.X.ToString() + "," + contourCenter.Y.ToString(), contourCenter + new OpenCvSharp.Point(-50, -10), HersheyFonts.HersheySimplex, 1, Scalar.FromRgb(250, 50, 50), 2);

            //Cv2.ImShow("Imagine compusa", imgComposed);

            // Draw actuator position line - Either axis X, either Y, either both
            foreach (Enums.Axis axis in TASettings.ConnectedAxisList)
                if (axis == Enums.Axis.X)
                    Cv2.Line(imgComposed, new OpenCvSharp.Point(actuatorPositionPixels.X, 0), new OpenCvSharp.Point(actuatorPositionPixels.X, imgComposed.Height - 1), Scalar.FromRgb(0, 250, 100), 3);

                else if (axis == Enums.Axis.Y)
                    Cv2.Line(imgComposed, new OpenCvSharp.Point(0, actuatorPositionPixels.Y), new OpenCvSharp.Point(imgComposed.Width - 1, actuatorPositionPixels.Y), Scalar.FromRgb(0, 250, 100), 3);

            return imgComposed;
        }

        public Mat ComposeImageIDC(Mat img, Mat[] allContours, List<OpenCvSharp.Point> pointsList, int distance)
        {
            Mat imgComposed = new Mat();

            Cv2.CvtColor(img, imgComposed, ColorConversionCodes.GRAY2BGR);
            Cv2.DrawContours(imgComposed, allContours, -1, Scalar.FromRgb(0, 128, 255), 2);

            for (int i = 0; i < allContours.Count(); i++)
                Cv2.Circle(imgComposed, pointsList[i], 2, Scalar.FromRgb(250, 50, 50), 5);

            Cv2.Line(imgComposed, pointsList[0], pointsList[1], Scalar.FromRgb(0, 250, 100), 1);
            Cv2.PutText(imgComposed, distance.ToString() + " px", new OpenCvSharp.Point((pointsList[0].X + pointsList[1].X) / 2 - 10, (pointsList[0].Y + pointsList[1].Y) / 2 - 20), HersheyFonts.HersheySimplex, 1, Scalar.FromRgb(250, 50, 50), 2);

            //Cv2.ImShow("Imagine compusa", imgComposed);

            return imgComposed;
        }

        public static VideoCapture Capture { get => capture; set => capture = value; }
        public ThresholdingAlgorithmsSettings TASettings { get => taSettings; set => taSettings = value; }
        public ImageDistanceCalibrationSettings IDCSettings { get => idcSettings; set => idcSettings = value; }
        public TrackingSettings TrackingSettings { get => trackingSettings; set => trackingSettings = value; }
        public VideoFeedSettings VideoFeedSettings { get => videoFeedSettings; set => videoFeedSettings = value; }
    }
}
