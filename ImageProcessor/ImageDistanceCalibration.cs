﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenCvSharp;
using Entities;


namespace ImageProcessor
{
    class ImageDistanceCalibration
    {
        ImageProcessingCore IPCore;
        List<OpenCvSharp.Point> pointsList;

        public ImageDistanceCalibration(ImageProcessingCore ipc)
        {
            this.IPCore = ipc;
            this.pointsList = new List<OpenCvSharp.Point>();
        }

        public (Bitmap, int) Run()
        {
            // Check is Video Capture hasn't been previously disposed, and start it if it has
            IPCore.IsVideoCaptureDisposed();

            // Try reading frame from capture device
            (Mat frame, bool isFrameValid) = IPCore.TryReadVideoCaptureFrame();

            if (isFrameValid == true)
            {
                Cv2.WaitKey(1);

                //Set image processing parameters
                IPCore.SetImageProcessingParameters();

                // Call processing algorthm - C# 7 tuple syntax return
                (Mat processedImage, int pixelDistance) = ProcessImage(frame);

                // Conversions & return
                return (OpenCvSharp.Extensions.BitmapConverter.ToBitmap(processedImage), pixelDistance);
            }
            else return (null, 0);
        }

        // C# 7 tuple syntax
        public (Mat, int) ProcessImage(Mat frame)
        {
            // Crop image to square
            Mat croppedImg = IPCore.CropToROI(frame);

            // Convert image to grayscale
            Mat imgGrayscale = IPCore.ConvertToGrayscale(croppedImg);

            // If checkBoxVideoFeedShowProcessedImage is not checked, return cropped and grayscale image
            if (IPCore.VideoFeedSettings.IsProcessedImageFed == false)
                return (imgGrayscale, 0);

            // Set image brightness if necessary
            if (IPCore.TASettings.Brightness != 0)
                imgGrayscale = IPCore.SetImageBrightness(imgGrayscale);

            // Blur grayscale image
            Mat imgBlurred = IPCore.SimpleImageBlur(imgGrayscale);

            // Calculate image histogram
            Mat histogram = IPCore.CalculateHistogram(imgBlurred);

            // Get maximum histogram value
            double maxHistValue = IPCore.FindMaxHistogramValue(histogram);

            // Get index of maximum value of histogram
            int maxHistIndex = IPCore.FindMaxHistogramIndex(histogram, maxHistValue);

            // Get value right of histogram, which is smaller or equal that the maximum value divided by a predefined value
            int dividedValIndex = IPCore.FindHistogramDividedIndex(histogram, Enums.HistogramCalculation.Reversed, maxHistValue, maxHistIndex);

            // Get threshold value & apply right offset if necessary
            int thresholdValue = IPCore.GetThresholdValue(dividedValIndex);

            // Apply threshold filter
            Mat imgThreshold = IPCore.ThresholdFilter(imgBlurred, thresholdValue, ThresholdTypes.Binary);

            // Invert Image
            Mat invertedThreshold = IPCore.InvertImage(imgThreshold);

            // Apply median filter
            Mat imgMedian = IPCore.MedianFilter(invertedThreshold);

            // Find all contours from image
            Mat[] allContours = IPCore.FindAllContours(imgMedian);

            // Find largest 2 contors
            Mat[] largestTwoContours = IPCore.FindLargestTwoContours(imgMedian, allContours);

            if (largestTwoContours.Count() == 2)
            {
                this.pointsList = new List<OpenCvSharp.Point>();

                foreach (Mat contour in largestTwoContours)
                    pointsList.Add(IPCore.FindContourCenter(imgMedian, contour));

                double distance = IPCore.CalculateEuclidianDistance(pointsList);

                return (IPCore.ComposeImageIDC(imgGrayscale, largestTwoContours, pointsList, (int)distance), (int)distance);
            }
            else return (imgGrayscale, 0);
        }
    }
}

