﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessor
{
    public class VideoFeedSettings
    {
        private bool isEnabled;
        private bool isProcessedImageFed;
        private int frameCount;
        private float frameTimeMs;
        private float framesPerSecond;
        private int framesPerSecondUpdateSkip;
        private int imageHeight;
        private int imageWidth;

        public VideoFeedSettings()
        {
            this.isEnabled = false;
            this.IsProcessedImageFed = true;
            this.frameCount = 0;
            this.frameTimeMs = 0f;
            this.framesPerSecond = 0f;
            this.FramesPerSecondUpdateSkip = 5;
            this.ImageHeight = 0;
            this.imageWidth = 0;
        }

        public VideoFeedSettings(bool isEnabled, bool isProcessedImageFed, int frameCount, float frameTimeMs, float framesPerSecond, int framesPerSecondUpdateSkip, int imageHeight, int imageWidth)
        {
            this.isEnabled = isEnabled;
            this.isProcessedImageFed = isProcessedImageFed;
            this.frameCount = frameCount;
            this.frameTimeMs = frameTimeMs;
            this.framesPerSecond = framesPerSecond;
            this.framesPerSecondUpdateSkip = framesPerSecondUpdateSkip;
            this.imageHeight = imageHeight;
            this.imageWidth = imageWidth;
        }

        public float CalculateFramesPerSecond(float frameTimeMs)
        {
            return 1000 / frameTimeMs;
        }

        public bool IsEnabled { get => isEnabled; set => isEnabled = value; }
        public bool IsProcessedImageFed { get => isProcessedImageFed; set => isProcessedImageFed = value; }
        public int FrameCount { get => frameCount; set => frameCount = value; }
        public float FrameTimeMs { get => frameTimeMs; set => frameTimeMs = value; }
        public float FramesPerSecond { get => framesPerSecond; set => framesPerSecond = value; }
        public int FramesPerSecondUpdateSkip { get => framesPerSecondUpdateSkip; set => framesPerSecondUpdateSkip = value; }
        public int ImageHeight { get => imageHeight; set => imageHeight = value; }
        public int ImageWidth { get => imageWidth; set => imageWidth = value; }
    }
}
