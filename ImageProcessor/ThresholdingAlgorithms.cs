﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenCvSharp;
using Entities;

namespace ImageProcessor
{
    /*
    alpha 1  beta 0      --> no change  
    0 < alpha < 1        --> lower contrast  
    alpha > 1            --> higher contrast  
    -127 < beta < +127   --> good range for brightness values
    */

    public class ThresholdingAlgorithms
    {
        ImageProcessingCore IPCore;
        static List<float> frameProcTimeList = new List<float>();


        public ThresholdingAlgorithms(ImageProcessingCore ipc)
        {
            this.IPCore = ipc;
        }

        public (Bitmap, System.Drawing.Point) Run(System.Drawing.Point actuatorPositionPixels)
        {
            // Check is Video Capture isn't disposed, start it if it's disposed
            IPCore.IsVideoCaptureDisposed();

            // Try reading frame from capture device
            (Mat frame, bool isFrameValid) = IPCore.TryReadVideoCaptureFrame();

            if (isFrameValid == true)
            {
                Cv2.WaitKey(1);

                //Set image processing parameters
                IPCore.SetImageProcessingParameters();

                // Return frame is no algorithm is selected
                if (IPCore.TASettings.Algorithm == Enums.ImgProcAlgorithm.None)
                    return (OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame), new System.Drawing.Point(int.MinValue, int.MinValue));


                /* ACQ FRAME*/
                //Stopwatch frameProcTime = new Stopwatch();
                //frameProcTime.Start();

                // Call processing algorthm - C# 7 tuple syntax return
                (Mat processedImage, OpenCvSharp.Point centerPointCV) = ProcessImage(frame, actuatorPositionPixels);

                //frameProcTime.Stop();
                //float time_us = frameProcTime.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
                //frameProcTimeList.Add(time_us / 1000);
                //Console.WriteLine("Timp mediu procesare img: " + frameProcTimeList.Average() + " now = " + time_us/ 1000);
                /* */



                // Conversions & return
                return (OpenCvSharp.Extensions.BitmapConverter.ToBitmap(processedImage), new System.Drawing.Point() { X = centerPointCV.X, Y = centerPointCV.Y });
            }
            else return (null, new System.Drawing.Point());
        }

        // C# 7 tuple syntax
        public (Mat, OpenCvSharp.Point) ProcessImage(Mat frame, System.Drawing.Point actuatorPositionPixels)
        {
            // Crop image to square
            Mat croppedImg = IPCore.CropToROI(frame);

            // Convert image to grayscale
            Mat imgGrayscale = IPCore.ConvertToGrayscale(croppedImg);

            // Invert image, if necessary
            if (IPCore.TASettings.IsInverted == true)
                imgGrayscale = IPCore.InvertImage(imgGrayscale);

            // If checkBoxVideoFeedShowProcessedImage is not checked, return grayscale image
            if (IPCore.VideoFeedSettings.IsProcessedImageFed == false)
                return (imgGrayscale, new OpenCvSharp.Point(int.MinValue, int.MinValue));

            // Set image brightness if necessary
            if (IPCore.TASettings.Brightness != 0)
                imgGrayscale = IPCore.SetImageBrightness(imgGrayscale);

            // Blur grayscale image
            Mat imgBlurred = IPCore.SimpleImageBlur(imgGrayscale);

            Mat imgPreContours = new Mat();

            if (IPCore.TASettings.Algorithm == Enums.ImgProcAlgorithm.StaticThresh)
            {
                Mat imgThreshold = IPCore.ThresholdFilter(imgBlurred, IPCore.TASettings.StaticThresholdValue, ThresholdTypes.Binary);

                imgPreContours = IPCore.MedianFilter(imgThreshold);
            }

            else if (IPCore.TASettings.Algorithm == Enums.ImgProcAlgorithm.DynamicThresh)
            {
                // Calculate image histogram
                Mat histogram = IPCore.CalculateHistogram(imgBlurred);

                // Get maximum histogram value
                double maxHistValue = IPCore.FindMaxHistogramValue(histogram);

                // Get index of maximum value of histogram
                int maxHistIndex = IPCore.FindMaxHistogramIndex(histogram, maxHistValue);

                // Get value right of histogram, which is smaller or equal that the maximum value divided by a predefined value
                int dividedValIndex = IPCore.FindHistogramDividedIndex(histogram, Enums.HistogramCalculation.Normal, maxHistValue, maxHistIndex);

                // Get threshold value & apply right offset if necessary
                int thresholdValue = IPCore.GetThresholdValue(dividedValIndex);

                // Apply threshold filter
                Mat imgThreshold = IPCore.ThresholdFilter(imgBlurred, thresholdValue, ThresholdTypes.Binary);

                // Apply median filter
                imgPreContours = IPCore.MedianFilter(imgThreshold);
            }
            else if (IPCore.TASettings.Algorithm == Enums.ImgProcAlgorithm.OstuThresh)
            {
                imgPreContours = IPCore.ThresholdFilter(imgBlurred, IPCore.TASettings.StaticThresholdValue, ThresholdTypes.Otsu);
            }

            return ProcessContours(imgPreContours, imgGrayscale, actuatorPositionPixels);
        }

        public (Mat, OpenCvSharp.Point) ProcessContours(Mat imgPreContours, Mat imgGrayscale, System.Drawing.Point actuatorPositionPixels)
        {
            // Find all contours from image
            Mat[] allContours = IPCore.FindAllContours(imgPreContours);

            if (allContours.Count() > 0)
            {
                // Get the contours that has the largest area
                Mat largestContourArea = IPCore.FindLargestContourArea(imgPreContours, allContours);

                // Get largest contour's area center point imgPreContours
                OpenCvSharp.Point contourCenter = IPCore.FindContourCenter(imgPreContours, largestContourArea);

                // Put center coordinates on image and return it
                return (IPCore.ComposeImageDTC(imgGrayscale, largestContourArea, contourCenter, actuatorPositionPixels), contourCenter);
            }
            else

                // Return the grayscale image if no contours were found
                return (imgGrayscale, new OpenCvSharp.Point(int.MinValue, int.MinValue));
        }
    }
}

//Console.WriteLine(s.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L)));