﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;
using Entities;

namespace View.UserControls
{
    public class MoveContinuouslyController : UserControl
    {
        MainControlsPanel mcp;
        MainController controller;
        ActuatorPositionSoftwareLimits apsl;

        public MoveContinuouslyController(MainControlsPanel mcp, MainController controller, ActuatorPositionSoftwareLimits apsl)
        {
            this.mcp = mcp;
            this.controller = controller;
            this.apsl = apsl;
        }

        public int GetListIndexByAxis(Enums.Axis axis)
        {
            foreach (ActuatorController actuator in Actuators.List)
                if (actuator.Axis == axis)
                    return actuator.ListIndex;

            return 0;
        }

        public void Retract(int listIndex)
        {
            if (mcp.TryConfiguringActuatorSettingsOneDevice(listIndex) == true)
                controller.ActuatorMoveContinuouslyLeft(controller.ActuatorInContext.DeviceID, apsl.MinEdgePositionStepsAllDevices);
        }

        public void SoftStop(int listIndex)
        {
            controller.ChangeContext(listIndex);
            controller.ActuatorSoftStop(controller.ActuatorInContext.DeviceID);
        }

        public void Expand(int listIndex)
        {
            if (mcp.TryConfiguringActuatorSettingsOneDevice(listIndex) == true)
                controller.ActuatorMoveContinuouslyRight(controller.ActuatorInContext.DeviceID, apsl.MaxEdgePositionStepsAllDevices);

        }
    }
}
