﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;
using Entities;

namespace View.UserControls
{
    public partial class ActuatorMoveContinuouslyAxisX : UserControl
    {
        private MoveContinuouslyController mcc;
        private int listIndex;

        public ActuatorMoveContinuouslyAxisX() { }

        public ActuatorMoveContinuouslyAxisX(MainController controller, ActuatorPositionSoftwareLimits apsl, MainControlsPanel mcp)
        {
            InitializeComponent();
            this.mcc = new MoveContinuouslyController(mcp, controller, apsl);
            this.listIndex = mcc.GetListIndexByAxis(Enums.Axis.X);
        }

        private void ButtonRetractAxisX_Click(object sender, RoutedEventArgs e)
        {
            mcc.Retract(listIndex);
        }

        private void ButtonStopAxisX_Click(object sender, RoutedEventArgs e)
        {
            mcc.SoftStop(listIndex);
        }

        private void ButtonExpandAxisX_Click(object sender, RoutedEventArgs e)
        {
            mcc.Expand(listIndex);
        }
    }
}
