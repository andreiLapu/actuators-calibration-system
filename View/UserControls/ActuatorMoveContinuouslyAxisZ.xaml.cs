﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;
using Entities;

namespace View.UserControls
{
    public partial class ActuatorMoveContinuouslyAxisZ : UserControl
    {
        private MoveContinuouslyController mcc;
        private int listIndex;

        public ActuatorMoveContinuouslyAxisZ() { }

        public ActuatorMoveContinuouslyAxisZ(MainController controller, ActuatorPositionSoftwareLimits apsl, MainControlsPanel mcp)
        {
            InitializeComponent();
            this.mcc = new MoveContinuouslyController(mcp, controller, apsl);
            this.listIndex = mcc.GetListIndexByAxis(Enums.Axis.Z);
        }

        private void ButtonRetractAxisZ_Click(object sender, RoutedEventArgs e)
        {
            mcc.Retract(listIndex);
        }

        private void ButtonStopAxisZ_Click(object sender, RoutedEventArgs e)
        {
            mcc.SoftStop(listIndex);
        }

        private void ButtonExpandAxisZ_Click(object sender, RoutedEventArgs e)
        {
            mcc.Expand(listIndex);
        }
    }
}
