﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;

namespace View.UserControls
{
    /// <summary>
    /// Interaction logic for ActuatorParametersConfigurationPanel.xaml
    /// </summary>
    public partial class ActuatorParametersConfigurationPanel : UserControl
    {
        List<ActuatorController> actuators;

        public ActuatorParametersConfigurationPanel(List<ActuatorController> actuators)
        {
            InitializeComponent();

            this.actuators = actuators;
        }
    }
}
