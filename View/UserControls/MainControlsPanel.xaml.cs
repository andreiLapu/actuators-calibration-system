﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Entities;
using Controller;
using ImageProcessor;
using ximc;
using System.ComponentModel;
using System.Threading;
using System.Drawing;
using System.Windows.Interop;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace View.UserControls
{
    /// <summary>
    /// Interaction logic for MainControlsPanel.xaml
    /// </summary>
    public partial class MainControlsPanel : UserControl
    {

        #region Members

        MainController controller;

        ApplicationConfigurationManager appConfigManager;
        ActuatorPositionSoftwareLimits apsl;

        BackgroundWorker bgWorkerGetStatsForUI;
        BackgroundWorker bgWorkerFeedProcessedImage;
        BackgroundWorker bgWorkerNotificationPolling;

        ImageProcessingCore IPcore;

        UIBrushes uiBrushes;

        int deviceCount;
        bool virtualMode = false;

        private int comboBoxOneIndex = -1;
        private int comboBoxTwoIndex = -1;
        private int comboBoxThreeIndex = -1;

        private static readonly Regex _signedRegex = new Regex("[^0-9-]");
        private static readonly Regex _unsignedRegex = new Regex("[^0-9]");

        int notificationsRowIndex = 1;
        //TO DO - ILog notifLogger = LogManager.GetLogger("NotificationLog");

        static List<float> frameProcTimeList = new List<float>();


        public ActuatorPositionSoftwareLimits Apsl { get => apsl; set => apsl = value; }
        public ApplicationConfigurationManager AppConfigManager { get => appConfigManager; set => appConfigManager = value; }

        #endregion Members

        #region Constructor

        public MainControlsPanel(MainController controller, int deviceCount)
        {
            InitializeComponent();
            this.controller = controller;
            this.IPcore = new ImageProcessingCore();
            this.uiBrushes = new UIBrushes();
            this.AppConfigManager = new ApplicationConfigurationManager(this);
            this.deviceCount = deviceCount;
            InitializeMainControlsPanel();
        }

        #endregion Constructor

        #region Background Workers

        void bgWorkerFeedProcessedImage_DoWork(object obj, DoWorkEventArgs e)
        {
            Bitmap bmp = null;
            BitmapImage bmpImage = null;
            System.Drawing.Point centerPoint = new System.Drawing.Point();
            int distance = 0;

            while (bgWorkerFeedProcessedImage.CancellationPending == false)
            {
                if (IPcore.VideoFeedSettings.IsEnabled == true)
                {
                    if (IPcore.IDCSettings.IsEnabled == true)
                        ImageDistanceCalibrationAlgorithm(bmp, bmpImage, distance);

                    else if (IPcore.TASettings.IsEnabled == true)
                    {
                        /* ACQ FRAME*/
                        Stopwatch frameProcTime = new Stopwatch();
                        frameProcTime.Start();

                        ThresholdingAlgorithms(bmp, bmpImage, centerPoint);

                        frameProcTime.Stop();
                        float time_us = frameProcTime.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
                        frameProcTimeList.Add(time_us / 1000);
                        Console.WriteLine("Timp mediu procesare img: " + frameProcTimeList.Average() + " now = " + time_us / 1000);
                        /* */
                    }

                    else
                        ThresholdingAlgorithms(bmp, bmpImage, centerPoint);
                }
            }
        }

        void bgWorkerFeedProcessedImage_ProgressChanged(object obj, ProgressChangedEventArgs e)
        {
            masterImg.Source = (BitmapImage)e.UserState;
        }

        void bgWorkerFeedProcessedImage_RunWorkerCompleted(object obj, RunWorkerCompletedEventArgs e)
        {
            masterImg.Source = null;
            textBlockVideoFeedFPS.Text = "FPS: 0";
        }

        void bgWorkerGetStatsForUI_DoWork(object obj, DoWorkEventArgs e)
        {
            while (bgWorkerGetStatsForUI.CancellationPending == false)
            {
                bgWorkerGetStatsForUI.ReportProgress(0);
                // LICENTA
                Thread.Sleep(100);
            }
        }

        // TO DO
        int device = -1;
        void bgWorkerGetStatsForUI_ProgressChanged(object obj, ProgressChangedEventArgs e)
        {
            device++;
            controller.ChangeContext(device);

            GetCurrentPositionForAxis(controller.ActuatorInContext);

            if (device == 0)
                GetStatsForUI(controller.ActuatorInContext, controller.ActuatorInContext.DeviceID, Enums.UIDevice.One);

            else if (device == 1)
                GetStatsForUI(controller.ActuatorInContext, controller.ActuatorInContext.DeviceID, Enums.UIDevice.Two);

            else if (device == 2)
                GetStatsForUI(controller.ActuatorInContext, controller.ActuatorInContext.DeviceID, Enums.UIDevice.Three);

            if (device == Actuators.List.Count - 1)
                device = -1;
        }

        void bgWorkerGetStatsForUI_RunWorkerCompleted(object obj, RunWorkerCompletedEventArgs e)
        {
            Notification.NotifList.Add(new Notification(Enums.NotificationType.CriticalError, "Cannot retrieve actuator data for UI, background worker shutting down"));
        }

        void bgWorkerNotificationPolling_DoWork(object obj, DoWorkEventArgs e)
        {
            while (bgWorkerNotificationPolling.CancellationPending == false)
            {
                // LICENTA
                bgWorkerNotificationPolling.ReportProgress(0);
                Thread.Sleep(50);
            }
        }

        void bgWorkerNotificationPolling_ProgressChanged(object obj, ProgressChangedEventArgs e)
        {
            // Notifications that are created outside the View are polled from here
            try
            {
                foreach (Notification notif in Notification.NotifList)
                {
                    if (!notif.WasDisplayed)
                    {
                        NotifyUI(notif.NotifType, notif.NotifString);
                        notif.WasDisplayed = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString()); /*TO DO*/
            }
        }

        void bgWorkerNotificationPolling_RunWorkerCompleted(object obj, RunWorkerCompletedEventArgs e)
        {

        }

        #endregion

        #region Events

        #region Sort these please

        private void CheckBoxActuatorSettingApplyForAll_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxActuatorSettingApplyForAll.IsChecked == true)
                ChangeActuatorSettingsDeviceCountUI(1);
            else
                ChangeActuatorSettingsDeviceCountUI(controller.Actuators.Count);
        }

        private void ComboBoxConnectionSettingsHandleSwap_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxConnectionSettingsHandleSwap();
        }

        // TO DO - aranajeaza
        private void CheckBoxMoveRelativelyApplyForAll_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxMoveRelativelyApplyForAll.IsChecked == true)
            {
                ShowGrid(gridMoveRelativelyFirstRowOneDevice);
                ShowGrid(gridMoveRelativelySecondRowOneDevice);
                ShowGrid(gridMoveRelativelyThirdRowOneDevice);

                HideGrid(gridMoveRelativelyFirstRowMultipleDevices);
                HideGrid(gridMoveRelativelySecondRowMultipleDevices);
                HideGrid(gridMoveRelativelyThirdRowMultipleDevices);

                rowDefinitionMoveRelativelyRowDeviceThree.Height = new GridLength(1, GridUnitType.Star);
            }
            else
            {
                HideGrid(gridMoveRelativelyFirstRowOneDevice);
                HideGrid(gridMoveRelativelySecondRowOneDevice);
                HideGrid(gridMoveRelativelyThirdRowOneDevice);

                ShowGrid(gridMoveRelativelyFirstRowMultipleDevices);
                ShowGrid(gridMoveRelativelySecondRowMultipleDevices);

                if (controller.Actuators.Count == 3)
                {
                    ShowGrid(gridMoveRelativelyThirdRowMultipleDevices);
                    rowDefinitionMoveRelativelyRowDeviceThree.Height = new GridLength(1, GridUnitType.Star);
                }
                else rowDefinitionMoveRelativelyRowDeviceThree.Height = new GridLength(0, GridUnitType.Star);
            }
        }

        private void ComboBoxMoveRelativelyDeviceOne_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HandleMoveRelativelyButtonTextChange(comboBoxMoveRelativelyDeviceOne, buttonMoveRelativelyDeviceOne);
            HandleCycleBetweenTextBlockChange(comboBoxMoveRelativelyDeviceOne, textBlockMoveRelativelyToOne);
        }

        private void ComboBoxMoveRelativelyDeviceTwo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HandleMoveRelativelyButtonTextChange(comboBoxMoveRelativelyDeviceTwo, buttonMoveRelativelyDeviceTwo);
            HandleCycleBetweenTextBlockChange(comboBoxMoveRelativelyDeviceTwo, textBlockMoveRelativelyToTwo);

        }

        private void ComboBoxMoveRelativelyDeviceThree_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HandleMoveRelativelyButtonTextChange(comboBoxMoveRelativelyDeviceThree, buttonMoveRelativelyDeviceThree);
            HandleCycleBetweenTextBlockChange(comboBoxMoveRelativelyDeviceThree, textBlockMoveRelativelyToThree);
        }

        #endregion

        #region Move Relatively One Device Events

        private void ButtonMoveToPositionOneDevice_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < Actuators.List.Count; i++)
                MoveToPositionOneDeviceEventHandling(textBoxMoveToPositionStepsOneDevice.Text, textBoxMoveToPositionMicroStepsOneDevice.Text, i);
        }

        private void ButtonShiftOnOneDevice_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < controller.Actuators.Count; i++)
                ShiftOnPositionOneDeviceEventhandling(controller.ActuatorInContext.DeviceID, textBoxShiftToStepsOneDevice.Text, textBoxShiftToMicroStepsOneDevice.Text, i);
        }

        // TO DO
        private void ButtonCycleBetweenOneDevice_Click(object sender, RoutedEventArgs e)
        {
            if (CheckCycleBetweenOneDeviceSafetyValues(int.Parse(textBoxCycleBeweenFromStepsOneDevice.Text), int.Parse(textBoxCycleBeweenToStepsOneDevice.Text)) == true)
                Console.WriteLine("Cycle correct");
            else Console.WriteLine("Cycle incorrect");
        }

        #endregion

        #region Background Worker (bgWorkerGetStatsForUI) Events

        private void GetStatsForUI(ActuatorController actuator, int deviceID, Enums.UIDevice uiDevice)
        {
            switch (uiDevice)
            {
                case Enums.UIDevice.One:
                    textBlockActuatorStatusSpeedDeviceOne.Text = controller.ActuatorInContext.GetCurrentSpeedSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroSpeedDeviceOne.Text = controller.ActuatorInContext.GetCurrentSpeedMicrosteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusPositionDeviceOne.Text = controller.ActuatorInContext.GetCurrentPositionSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroPositionDeviceOne.Text = controller.ActuatorInContext.GetCurrentPositionMicroSteps(controller.ActuatorInContext.DeviceID).ToString();

                    textBlockPowerStatusVoltageDeviceOne.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).Upwr / 100).ToString("0.00");
                    textBlockPowerStatusCurrentDeviceOne.Text = actuator.GetStatusCalibration(deviceID, actuator.Calibration).Ipwr.ToString();
                    textBlockPowerStatusTemperatureDeviceOne.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).CurT / 10).ToString("0");
                    break;

                case Enums.UIDevice.Two:
                    textBlockActuatorStatusSpeedDeviceTwo.Text = controller.ActuatorInContext.GetCurrentSpeedSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroSpeedDeviceTwo.Text = controller.ActuatorInContext.GetCurrentSpeedMicrosteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusPositionDeviceTwo.Text = controller.ActuatorInContext.GetCurrentPositionSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroPositionDeviceTwo.Text = controller.ActuatorInContext.GetCurrentPositionMicroSteps(controller.ActuatorInContext.DeviceID).ToString();

                    textBlockPowerStatusVoltageDeviceTwo.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).Upwr / 100).ToString("0.00");
                    textBlockPowerStatusCurrentDeviceTwo.Text = actuator.GetStatusCalibration(deviceID, actuator.Calibration).Ipwr.ToString();
                    textBlockPowerStatusTemperatureDeviceTwo.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).CurT / 10).ToString("0");
                    break;

                case Enums.UIDevice.Three:
                    textBlockActuatorStatusSpeedDeviceThree.Text = controller.ActuatorInContext.GetCurrentSpeedSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroSpeedDeviceThree.Text = controller.ActuatorInContext.GetCurrentSpeedMicrosteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusPositionDeviceThree.Text = controller.ActuatorInContext.GetCurrentPositionSteps(controller.ActuatorInContext.DeviceID).ToString();
                    textBlockActuatorStatusMicroPositionDeviceThree.Text = controller.ActuatorInContext.GetCurrentPositionMicroSteps(controller.ActuatorInContext.DeviceID).ToString();

                    textBlockPowerStatusVoltageDeviceThree.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).Upwr / 100).ToString("0.00");
                    textBlockPowerStatusCurrentDeviceThree.Text = actuator.GetStatusCalibration(deviceID, actuator.Calibration).Ipwr.ToString();
                    textBlockPowerStatusTemperatureDeviceThree.Text = (actuator.GetStatusCalibration(deviceID, actuator.Calibration).CurT / 10).ToString("0");
                    break;
            }
        }

        //TO DO - move
        int axisXPos = 0;
        int axisYPos = 0;
        private void GetCurrentPositionForAxis(ActuatorController actuator)
        {
            switch (actuator.Axis)
            {
                case Enums.Axis.X:
                    axisXPos = actuator.GetCurrentPositionSteps(actuator.DeviceID);
                    break;

                case Enums.Axis.Y:
                    axisYPos = actuator.GetCurrentPositionSteps(actuator.DeviceID);
                    break;
            }
        }

        #endregion

        private void ButtonMoveRelativelyDeviceOne_Click(object sender, RoutedEventArgs e)
        {
            HandleMoveRelativelyMultipleDevices(buttonMoveRelativelyDeviceOne, textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, 0);
        }

        private void ButtonMoveRelativelyDeviceTwo_Click(object sender, RoutedEventArgs e)
        {
            HandleMoveRelativelyMultipleDevices(buttonMoveRelativelyDeviceTwo, textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, 1);
        }

        private void ButtonMoveRelativelyDeviceThree_Click(object sender, RoutedEventArgs e)
        {
            HandleMoveRelativelyMultipleDevices(buttonMoveRelativelyDeviceThree, textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, 2);
        }

        private void ComboBoxTest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ArrangeComponents(comboBoxTest.SelectedIndex + 1);
        }

        #region Text Box Events

        // Handle textBox input (for unsigned values)
        private void TextBoxHandleUnsignedValue(object sender, TextCompositionEventArgs e)
        {
            e.Handled = _unsignedRegex.IsMatch(e.Text);
        }

        // Handle textBox input (for signed values)
        private void TextBoxHandleSignedValue(object sender, TextCompositionEventArgs e)
        {
            CheckSignedTextboxValidityAtPreviewInput(e);
        }

        // Disallow space key
        private void TextBoxSpaceKeyHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
        }

        // Make sure unallowed characters are not pasted into unsigned value textBoxes
        private void TextBoxUnsignedPastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                string text = (string)e.DataObject.GetData(typeof(string));

                if (_unsignedRegex.IsMatch(text) == true)
                    e.CancelCommand();
            }
            else
                e.CancelCommand();
        }

        // Make sure unallowed characters are not pasted into signed value textBoxes
        private void TextBoxSignedPastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                string text = (string)e.DataObject.GetData(typeof(string));

                if (_signedRegex.IsMatch(text) == true || CheckSignedTextboxValidityAtPaste(text, e) == false)
                    e.CancelCommand();
            }
            else
                e.CancelCommand();
        }

        private void TextBoxMoveRelativelyWarningHandler(object sender, TextChangedEventArgs e)
        {
            if (controller.Actuators.Count == 1)
            {
                if ((sender as TextBox).Name == "textBoxMoveToPositionStepsOneDevice" || (sender as TextBox).Name == "textBoxMoveToPositionMicroStepsOneDevice")
                    HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveToPositionStepsOneDevice, textBoxMoveToPositionMicroStepsOneDevice, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveToPositionOneDevice);

                else if ((sender as TextBox).Name == "textBoxShiftToStepsOneDevice" || (sender as TextBox).Name == "textBoxShiftToMicroStepsOneDevice")
                    HandleMoveRelativelyTextBoxTypingSafety(textBoxShiftToStepsOneDevice, textBoxShiftToMicroStepsOneDevice, Enums.TextBoxValueSafetyType.ShiftOn, buttonShiftOnOneDevice);

                else if ((sender as TextBox).Name == "textBoxCycleBeweenFromStepsOneDevice" || (sender as TextBox).Name == "textBoxCycleBeweenToStepsOneDevice")
                    HandleMoveRelativelyTextBoxTypingSafety(textBoxCycleBeweenFromStepsOneDevice, textBoxCycleBeweenToStepsOneDevice, Enums.TextBoxValueSafetyType.CycleBetween, buttonCycleBetweenOneDevice);
            }
            else if (controller.Actuators.Count > 1)
            {
                switch (comboBoxMoveRelativelyDeviceOne.SelectedIndex)
                {
                    case 0:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceOne);
                        break;

                    case 1:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceOne);
                        break;

                    case 2:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceOne);
                        break;
                }

                switch (comboBoxMoveRelativelyDeviceTwo.SelectedIndex)
                {
                    case 0:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceTwo);
                        break;

                    case 1:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceTwo);
                        break;

                    case 2:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceTwo);
                        break;
                }

                switch (comboBoxMoveRelativelyDeviceThree.SelectedIndex)
                {
                    case 0:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceThree);
                        break;

                    case 1:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceThree);
                        break;

                    case 2:
                        HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceThree);
                        break;
                }
            }
        }

        private void TextBoxActuatorSettingsSpeedValidityHandling(object sender, TextChangedEventArgs e)
        {
            HandleActuatorSettingsTextBoxTypingSafety(e.Source as TextBox, Enums.TextBoxActuatorSettingsType.Speed);
        }

        private void TextBoxActuatorSettingsMicroSpeedValidityHandling(object sender, TextChangedEventArgs e)
        {
            HandleActuatorSettingsTextBoxTypingSafety(e.Source as TextBox, Enums.TextBoxActuatorSettingsType.MicroSpeed);
        }

        private void TextBoxActuatorSettingsAccelerationValidityHandling(object sender, TextChangedEventArgs e)
        {
            HandleActuatorSettingsTextBoxTypingSafety(e.Source as TextBox, Enums.TextBoxActuatorSettingsType.Acceleration);
        }

        private void TextBoxActuatorSettingsDecelerationValidityHandling(object sender, TextChangedEventArgs e)
        {
            HandleActuatorSettingsTextBoxTypingSafety(e.Source as TextBox, Enums.TextBoxActuatorSettingsType.Deceleration);
        }

        #endregion

        #region Image Processing Settings Events

        private void SliderImageProcessingSettingsPrecision_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TASettings != null)
                IPcore.TASettings.Precision = IPcore.TASettings.CalculatePrecision(sliderImageProcessingSettingsPrecision.Value);
        }

        private void SliderImageProcessingSettingsStaticThreshold_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TASettings != null)
                IPcore.TASettings.StaticThresholdValue = (int)sliderImageProcessingSettingsStaticThreshold.Value;
        }

        private void SliderImageProcessingSettingsQuality_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TASettings != null)
                IPcore.TASettings.Quality = (int)sliderImageProcessingSettingsQuality.Value;
        }

        private void SliderImageProcessingSettingsBrightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TASettings != null)
                IPcore.TASettings.Brightness = (int)sliderImageProcessingSettingsBrightness.Value;
        }

        private void ButtonImageProcessingSettingsResetPrecision_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TASettings != null)
            {
                IPcore.TASettings.ResetPrecision();
                sliderImageProcessingSettingsPrecision.Value = IPcore.TASettings.DefaultPrecisionSliderValue;
            }
        }

        private void ButtonImageProcessingSettingsResetStaticThreshold_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TASettings != null)
            {
                sliderImageProcessingSettingsStaticThreshold.Value = (int)IPcore.TASettings.ResetStaticThresholdValue();
            }
        }

        private void ButtonImageProcessingSettingsResetQuality_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TASettings != null)
            {
                IPcore.TASettings.ResetQuality();
                sliderImageProcessingSettingsQuality.Value = IPcore.TASettings.DefaultQualitySliderValue;
            }
        }

        private void ButtonImageProcessingSettingsResetBrightness_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TASettings != null)
            {
                IPcore.TASettings.ResetBrightness();
                sliderImageProcessingSettingsBrightness.Value = IPcore.TASettings.DefaultBrightnessSliderValue;
            }
        }

        #endregion

        #region Tracking Settings Events

        private void SlideTrackingSettingsPixelTolerance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TrackingSettings != null)
                IPcore.TrackingSettings.PixelTolerance = (int)(sliderTrackingSettingsPixelTolerance.Value + 1);
        }

        private void SlideTrackingSettingsTrackingSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (IPcore != null && IPcore.TrackingSettings != null)
                IPcore.TrackingSettings.FrameSkipCount = (int)(sliderTrackingSettingsTrackingSpeed.Value + 1);
        }

        private void ButtonTrackingSettingsResetPixelTolerance_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TrackingSettings != null)
                sliderTrackingSettingsPixelTolerance.Value = IPcore.TrackingSettings.ResetPixelTolerance();
        }

        private void ButtonTrackingSettingsResetTrackingSpeed_Click(object sender, RoutedEventArgs e)
        {
            if (IPcore != null && IPcore.TrackingSettings != null)
                sliderTrackingSettingsTrackingSpeed.Value = IPcore.TrackingSettings.ResetFrameSkipCount() - 1;
        }

        #endregion

        #region Image Processing Events

        // TO DELETE
        private void ButtonApplyImgProc_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region Video Feed Events

        private void ButtonStartStopVideoFeed_Click(object sender, RoutedEventArgs e)
        {
            HandleVideoFeedStartStop(IPcore.VideoFeedSettings.IsEnabled, e);
        }

        private void ComboBoxVideoFeedAlgorithm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { HandleVideoFeedAlgorithmSelectionChange(); }
            catch { /*TO DO*/ }
        }

        private void ButtonStartStopTracking_Click(object sender, RoutedEventArgs e)
        {
            HandleTrackingStartStop(IPcore.TrackingSettings.IsEnabled, e);
        }

        private void ButtonCenterImage_Click(object sender, RoutedEventArgs e)
        {
            zoomBorder.Reset();
        }

        private void ButtonCaptureImage_Click(object sender, RoutedEventArgs e)
        {
            HandleVideoFeedCapture();
        }

        private void CheckBoxVideoFeedShowProcessedImage_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxVideoFeedShowProcessedImage.IsChecked == true)
                IPcore.VideoFeedSettings.IsProcessedImageFed = true;
            else
                IPcore.VideoFeedSettings.IsProcessedImageFed = false;
        }

        private void CheckBoxVideoFeedShowInvertImage_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxVideoFeedShowInvertImage.IsChecked == true)
                IPcore.TASettings.IsInverted = true;
            else
                IPcore.TASettings.IsInverted = false;
        }

        private void ButtonDistanceCalibrationSet_Click(object sender, RoutedEventArgs e)
        {
            CalculateCalibrationDistances(IPcore.IDCSettings.DistancePixels);
        }

        private void ButtonVideoFeedSettingsSet_Click(object sender, RoutedEventArgs e)
        {
            HandleVideoFeedImageSizeSettings();
        }

        #endregion

        #endregion Events

        #region Methods

        #region Initializers

        private void InitializeMainControlsPanel()
        {
            // TO DO  - comment
            AppConfigManager.LoadAllFromConfiguration();
            GetConnectionSettingsComboBoxSelectedItemIndex();
            ArrangeComponents(deviceCount);
            SetLabelsText();
            SetSliderValueBindings();

            InitializebgWorkers();
            CheckEveryTextBoxTypingSafety();

            IPcore.StartCapture();
            IPcore.TASettings.SetDefaultValues((int)sliderImageProcessingSettingsPrecision.Value,
                                 (int)sliderImageProcessingSettingsQuality.Value,
                                 (int)sliderImageProcessingSettingsBrightness.Value);
            IPcore.TASettings.SetConnectedAxis(controller.Actuators);
            IPcore.TrackingSettings.SetDefaultValues((int)sliderTrackingSettingsPixelTolerance.Value,
                                              (int)sliderTrackingSettingsTrackingSpeed.Value);

            HandleVideoFeedStartStop(IPcore.VideoFeedSettings.IsEnabled, null);
            HandleTrackingStartStop(IPcore.TrackingSettings.IsEnabled, null);
        }

        private void InitializebgWorkers()
        {
            bgWorkerGetStatsForUI = new BackgroundWorker();
            bgWorkerFeedProcessedImage = new BackgroundWorker();
            bgWorkerNotificationPolling = new BackgroundWorker();

            bgWorkerGetStatsForUI.DoWork += new DoWorkEventHandler(bgWorkerGetStatsForUI_DoWork);
            bgWorkerGetStatsForUI.ProgressChanged += new ProgressChangedEventHandler(bgWorkerGetStatsForUI_ProgressChanged);
            bgWorkerGetStatsForUI.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorkerGetStatsForUI_RunWorkerCompleted);
            bgWorkerGetStatsForUI.WorkerReportsProgress = true;
            bgWorkerGetStatsForUI.WorkerSupportsCancellation = true;

            bgWorkerFeedProcessedImage.DoWork += new DoWorkEventHandler(bgWorkerFeedProcessedImage_DoWork);
            bgWorkerFeedProcessedImage.ProgressChanged += new ProgressChangedEventHandler(bgWorkerFeedProcessedImage_ProgressChanged);
            bgWorkerFeedProcessedImage.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorkerFeedProcessedImage_RunWorkerCompleted);
            bgWorkerFeedProcessedImage.WorkerReportsProgress = true;
            bgWorkerFeedProcessedImage.WorkerSupportsCancellation = true;

            bgWorkerNotificationPolling.DoWork += new DoWorkEventHandler(bgWorkerNotificationPolling_DoWork);
            bgWorkerNotificationPolling.ProgressChanged += new ProgressChangedEventHandler(bgWorkerNotificationPolling_ProgressChanged);
            bgWorkerNotificationPolling.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorkerNotificationPolling_RunWorkerCompleted);
            bgWorkerNotificationPolling.WorkerReportsProgress = true;
            bgWorkerNotificationPolling.WorkerSupportsCancellation = true;

            bgWorkerGetStatsForUI.RunWorkerAsync();
            bgWorkerNotificationPolling.RunWorkerAsync();
            //bgWorkerFeedProcessedImage.RunWorkerAsync();
        }

        #endregion

        #region Image Processing

        private void ThresholdingAlgorithms(Bitmap bmp, BitmapImage bmpImage, System.Drawing.Point centerPoint)
        {
            // Processing time counter
            Stopwatch s = Stopwatch.StartNew();

            // Grab processed image and coordinates
            (bmp, centerPoint) = IPcore.StartThresholdingAlgorithms(MapStepsToImage());

            // Check if image returned is valid
            if (bmp != null)
            {
                // Convert image
                bmpImage = BitmapToBitmapImage(bmp);

                // Update video feed
                bgWorkerFeedProcessedImage.ReportProgress(0, bmpImage);

                // Release frozen bitmap - dirty, but prevents memory leak
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Tracking algorithm (both axis)
                TrackingPrototype(centerPoint, (int)bmpImage.Height, (int)bmpImage.Width);

                // Update image processing settings
                UpdateImageProcessingSettings(s, bmpImage);
            }

            // If frame is invalid (null)
            else
                HandleNullFrameReturn();
        }

        private void ImageDistanceCalibrationAlgorithm(Bitmap bmp, BitmapImage bmpImage, int distance)
        {
            // Processing time counter
            Stopwatch s = Stopwatch.StartNew();

            // Grab processed image and distance
            (bmp, distance) = IPcore.StartImageDistanceCalibration();

            // Check if image returned is valid
            if (bmp != null)
            {
                // Convert image
                bmpImage = BitmapToBitmapImage(bmp);

                // Check calibration button validity
                CheckImageCalibrationButtonValidity(distance);

                // Update image processing settings
                UpdateImageCalibrationSettingsPixelDistanceUI(distance);

                // Update video feed
                bgWorkerFeedProcessedImage.ReportProgress(0, bmpImage);

                // Release frozen bitmap - dirty, but prevents memory leak
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Update image processing settings
                UpdateImageProcessingSettings(s, bmpImage);
            }

            // If frame is invalid (null)
            else
                HandleNullFrameReturn();
        }

        private void UpdateImageProcessingSettings(Stopwatch s, BitmapImage bmpImage)
        {
            s.Stop();

            IPcore.VideoFeedSettings.FrameCount++;
            IPcore.VideoFeedSettings.FrameTimeMs = s.ElapsedMilliseconds;
            IPcore.VideoFeedSettings.FramesPerSecond = IPcore.VideoFeedSettings.CalculateFramesPerSecond(IPcore.VideoFeedSettings.FrameTimeMs);

            IPcore.VideoFeedSettings.ImageHeight = (int)bmpImage.Height + 1;
            IPcore.VideoFeedSettings.ImageWidth = (int)bmpImage.Width + 1;

            if (IPcore.VideoFeedSettings.FrameCount % IPcore.VideoFeedSettings.FramesPerSecondUpdateSkip == 0)
                try
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        textBlockVideoFeedFPS.Text = "FPS: " + IPcore.VideoFeedSettings.FramesPerSecond.ToString("0.00");
                    });
                }
                catch { /*TO DO*/ }
        }

        public BitmapImage BitmapToBitmapImage(Bitmap bitmap)
        {
            MemoryStream memoryStream = new MemoryStream();
            BitmapImage bmpImage = new BitmapImage();

            bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Bmp);

            bmpImage.BeginInit();
            memoryStream.Seek(0, SeekOrigin.Begin);
            bmpImage.StreamSource = memoryStream;
            bmpImage.EndInit();
            bmpImage.Freeze();

            return bmpImage;
        }

        private void HandleNullFrameReturn()
        {
            IPcore.StopCapture();
            HandleVideoFeedStartStop(IPcore.VideoFeedSettings.IsEnabled = false, null);
            HandleTrackingStartStop(IPcore.TrackingSettings.IsEnabled = false, null);
            Notification.NotifList.Add(new Notification(Enums.NotificationType.CriticalError, "Could not retrieve frame from capturing device. Please check connection"));
        }

        #endregion

        #region Video Feed Start/Stop handling

        public void HandleVideoFeedStartStop(bool isVideoFeedEnabled, RoutedEventArgs e)
        {
            // If the routed event e is null => the method is not called from firing an event
            // If the routed event e is not null => the method is called from firing an event
            if (e != null)
            {
                if (isVideoFeedEnabled == true)
                {
                    IPcore.VideoFeedSettings.IsEnabled = false;
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Video feed was stopped"));
                }
                else
                {
                    IPcore.VideoFeedSettings.IsEnabled = true;
                    (int actualFrameWidth, int actualFrameheight) = IPcore.GetResolution();
                    textBoxVideoFeedSettingsActualSize.Text = actualFrameWidth + "x" + actualFrameheight;
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Video feed was started"));
                }
            }
            else IPcore.VideoFeedSettings.IsEnabled = isVideoFeedEnabled;

            StartStopVideoFeed(IPcore.VideoFeedSettings.IsEnabled);
            ChangeStartStopVideoFeedButtonAppearance(IPcore.VideoFeedSettings.IsEnabled);
            DisableVideoFeedControlsTA(IPcore.VideoFeedSettings.IsEnabled);
        }

        private void StartStopVideoFeed(bool isVideoFeedEnabled)
        {
            if (isVideoFeedEnabled == true)
                bgWorkerFeedProcessedImage.RunWorkerAsync();
            else
            {
                bgWorkerFeedProcessedImage.CancelAsync();
                HandleTrackingStartStop(false, null);
            }
        }

        private void ChangeStartStopVideoFeedButtonAppearance(bool isVideoFeedEnabled)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (isVideoFeedEnabled == true)
                    {
                        buttonStartStopVideoFeed.Content = "Stop feed";
                        buttonStartStopVideoFeed.Background = uiBrushes.BordeauxAccentLight;
                    }
                    else
                    {
                        buttonStartStopVideoFeed.Content = "Start feed";
                        buttonStartStopVideoFeed.Background = uiBrushes.GreenStart;
                    }
                });
            }
            catch { /*TO DO*/ }
        }

        private void DisableVideoFeedControlsTA(bool isVideoFeedEnabled)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (isVideoFeedEnabled == false)
                    {
                        HandleTrackingStartStop(IPcore.TrackingSettings.IsEnabled = false, null);

                        buttonStartStopTracking.Background = uiBrushes.Gray;
                        buttonStartStopTracking.IsEnabled = false;

                        checkBoxVideoFeedShowProcessedImage.IsEnabled = false;
                    }
                    else
                    {
                        buttonStartStopTracking.Background = uiBrushes.GreenStart;
                        buttonStartStopTracking.IsEnabled = true;

                        checkBoxVideoFeedShowProcessedImage.IsEnabled = true;
                    }
                });
            }
            catch { /*TO DO*/ }
        }

        #endregion

        #region Tracking Start/Stop handling

        private void HandleTrackingStartStop(bool isTrackingEnabled, RoutedEventArgs e)
        {
            // If the routed event e is null => the method is not called from firing an event
            // If the routed event e is not null => the method is called from firing an event
            if (e != null)
            {
                if (isTrackingEnabled == true)
                {
                    IPcore.TrackingSettings.IsEnabled = false;
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Tracking was stopped"));
                }
                else
                {
                    IPcore.TrackingSettings.IsEnabled = true;
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Tracking was started"));
                    checkBoxVideoFeedShowProcessedImage.IsChecked = true;
                    IPcore.VideoFeedSettings.IsProcessedImageFed = true;
                }
            }
            else
                IPcore.TrackingSettings.IsEnabled = isTrackingEnabled;

            ChangeStartStopTrackingButtonAppearance(IPcore.TrackingSettings.IsEnabled);
        }

        private void ChangeStartStopTrackingButtonAppearance(bool isVideoFeedEnabled)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (isVideoFeedEnabled == true)
                    {
                        buttonStartStopTracking.Content = "Stop tracking";
                        buttonStartStopTracking.Background = uiBrushes.BordeauxAccentLight;
                    }
                    else if (buttonStartStopTracking.IsEnabled == true)
                    {
                        buttonStartStopTracking.Content = "Start tracking";
                        buttonStartStopTracking.Background = uiBrushes.GreenStart;
                    }
                    else
                    {
                        buttonStartStopTracking.Content = "Start tracking";
                        buttonStartStopTracking.Background = uiBrushes.Gray;
                    }
                });
            }
            catch { /*TO DO*/ }
        }

        #endregion

        #region ComboBox Selection Changed Algorithm handling

        public void HandleVideoFeedAlgorithmSelectionChange()
        {
            switch (comboBoxVideoFeedAlgorithm.SelectedIndex)
            {
                // No algorithm
                case 0:
                    IPcore.TASettings.Algorithm = Enums.ImgProcAlgorithm.None;
                    HandleNoneVideoFeedAlgorithmChange();
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "No image processing algorithm was selected"));
                    break;

                // Static threshold algorithm
                case 1:
                    IPcore.TASettings.Algorithm = Enums.ImgProcAlgorithm.StaticThresh;
                    HandleTAVideoFeedAlgorithmChange();
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Static thresholding algorithm was selected"));
                    break;

                // Dynamic threshold contour algorithm
                case 2:
                    IPcore.TASettings.Algorithm = Enums.ImgProcAlgorithm.DynamicThresh;
                    HandleTAVideoFeedAlgorithmChange();
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Dynamic thresholding algorithm was seleceted"));
                    break;

                // Otsu threshold
                case 3:
                    IPcore.TASettings.Algorithm = Enums.ImgProcAlgorithm.OstuThresh;
                    HandleTAVideoFeedAlgorithmChange();
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Otsu thresholding algorithm was seleceted"));
                    break;

                // IDC (Image Distance Calibration) algorithm
                case 4:
                    IPcore.TASettings.Algorithm = Enums.ImgProcAlgorithm.DistCalibration;
                    HandleIDCVideoFeedAlgorithmChange();
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Camera, "Image distance calibration algorithm was selected"));
                    break;
            }

            SwapPrecisionAndStaticThresholdControls();
        }

        public void HandleNoneVideoFeedAlgorithmChange()
        {
            // Enable/disable image processing algorithms
            IPcore.TASettings.IsEnabled = false;
            IPcore.IDCSettings.IsEnabled = false;
            HandleTrackingStartStop(false, null);

            // Show/hide/rescale UI elements
            buttonStartStopTracking.Visibility = Visibility.Hidden;
            buttonStartStopTracking.Height = 0;
            buttonStartStopTracking.Margin = new Thickness(0);
            checkBoxVideoFeedShowInvertImage.Visibility = Visibility.Hidden;
            checkBoxVideoFeedShowInvertImage.Height = 0;
            checkBoxVideoFeedShowInvertImage.Margin = new Thickness(0);
            checkBoxVideoFeedShowProcessedImage.Visibility = Visibility.Hidden;
            checkBoxVideoFeedShowProcessedImage.Height = 0;
            checkBoxVideoFeedShowInvertImage.Margin = new Thickness(0);

            // Show/hide settings panel
            gridTrackingSettings.Visibility = Visibility.Visible;
            gridDistanceCalibration.Visibility = Visibility.Hidden;
        }

        public void HandleTAVideoFeedAlgorithmChange()
        {
            // Enable/disable image processing algorithms
            IPcore.TASettings.IsEnabled = true;
            IPcore.IDCSettings.IsEnabled = false;
            HandleTrackingStartStop(false, null);

            // Show/hide/rescale UI elements
            buttonStartStopTracking.Visibility = Visibility.Visible;
            buttonStartStopTracking.Height = 32;
            buttonStartStopTracking.Margin = new Thickness(5, 0, 5, 5);
            checkBoxVideoFeedShowInvertImage.Visibility = Visibility.Visible;
            checkBoxVideoFeedShowInvertImage.Height = 19;
            checkBoxVideoFeedShowInvertImage.Margin = new Thickness(5, 0, 5, 5);
            checkBoxVideoFeedShowProcessedImage.Visibility = Visibility.Visible;
            checkBoxVideoFeedShowProcessedImage.Height = 19;
            checkBoxVideoFeedShowProcessedImage.Margin = new Thickness(5, 0, 0, 0);

            // Show/hide settings panel
            gridTrackingSettings.Visibility = Visibility.Visible;
            gridDistanceCalibration.Visibility = Visibility.Hidden;
        }

        public void HandleIDCVideoFeedAlgorithmChange()
        {
            // Enable/disable image processing algorithms
            IPcore.TASettings.IsEnabled = false;
            IPcore.IDCSettings.IsEnabled = true;
            HandleTrackingStartStop(false, null);

            // Show/hide/rescale UI elements
            buttonStartStopTracking.Visibility = Visibility.Hidden;
            buttonStartStopTracking.Height = 0;
            buttonStartStopTracking.Margin = new Thickness(0);

            checkBoxVideoFeedShowInvertImage.Visibility = Visibility.Hidden;
            checkBoxVideoFeedShowInvertImage.Height = 0;
            checkBoxVideoFeedShowInvertImage.Margin = new Thickness(0);
            checkBoxVideoFeedShowProcessedImage.Visibility = Visibility.Visible;
            checkBoxVideoFeedShowProcessedImage.Height = 19;
            checkBoxVideoFeedShowProcessedImage.Margin = new Thickness(5, 0, 0, 0);

            // Show/hide settings panel
            gridTrackingSettings.Visibility = Visibility.Hidden;
            gridDistanceCalibration.Visibility = Visibility.Visible;
        }

        private void SwapPrecisionAndStaticThresholdControls()
        {
            if (IPcore.TASettings.Algorithm == Enums.ImgProcAlgorithm.StaticThresh)
            {
                sliderImageProcessingSettingsPrecision.Visibility = Visibility.Hidden;
                buttonImageProcessingSettingsResetPrecision.Visibility = Visibility.Hidden;

                sliderImageProcessingSettingsStaticThreshold.Visibility = Visibility.Visible;
                buttonImageProcessingSettingsResetStaticThreshold.Visibility = Visibility.Visible;
                textBlockImageProcessingSettingsPrecision.Text = "Threshold";
            }
            else
            {
                sliderImageProcessingSettingsPrecision.Visibility = Visibility.Visible;
                buttonImageProcessingSettingsResetPrecision.Visibility = Visibility.Visible;
                textBlockImageProcessingSettingsPrecision.Text = "Precision";

                sliderImageProcessingSettingsStaticThreshold.Visibility = Visibility.Hidden;
                buttonImageProcessingSettingsResetStaticThreshold.Visibility = Visibility.Hidden;
            }
        }

        #endregion

        #region Image Distance Calibration handling

        private void CheckImageCalibrationButtonValidity(int distance)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (distance < 1 || textBoxDistanceCalibrationKnownDistance.Text == "" || IPcore.VideoFeedSettings.IsEnabled == false)
                    {
                        buttonDistanceCalibrationSet.IsEnabled = false;
                        buttonDistanceCalibrationSet.Background = uiBrushes.Gray;
                    }
                    else
                    {
                        buttonDistanceCalibrationSet.IsEnabled = true;
                        buttonDistanceCalibrationSet.Background = uiBrushes.GreenStart;
                    }
                });
            }
            catch { /*TO DO*/}
        }

        private void CalculateCalibrationDistances(int distance)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    IPcore.IDCSettings.CalculateMilimetersPerPixel(int.Parse(textBoxDistanceCalibrationKnownDistance.Text), distance);
                    IPcore.IDCSettings.CalculateTranslatedDistanceMilimeters();
                    IPcore.IDCSettings.CalculateStepsPerPixel();

                    textBlockDistanceCalibrationMilimetersPerPixelValue.Text = IPcore.IDCSettings.MilimetersPerPixel.ToString("0.000") + " mm";
                    textBlockDistanceCalibrationStepsPerPixelValue.Text = IPcore.IDCSettings.StepsPerPixel.ToString("0.00") + " st";

                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Success, "Image distance was successfully calibrated"));
                });
            }
            catch { /*TO DO*/ }
        }

        private void UpdateImageCalibrationSettingsPixelDistanceUI(int distance)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    IPcore.IDCSettings.DistancePixels = distance;
                    textBlockDistanceCalibrationPixelDistanceValue.Text = IPcore.IDCSettings.DistancePixels.ToString() + " px";
                });
            }
            catch { /*TO DO*/ }
        }

        #endregion

        #region Video Feed Settings Image Size handling

        // TO DO - clean ASAP
        private void HandleVideoFeedImageSizeSettings()
        {
            if (textBoxVideoFeedSettingsFrameWidth.Text != "" && textBoxVideoFeedSettingsFrameHeight.Text != "")
            {
                if (textBoxVideoFeedSettingsFrameWidth.Text != "0" && textBoxVideoFeedSettingsFrameHeight.Text != "0")
                {
                    IPcore.VideoFeedSettings.ImageWidth = int.Parse(textBoxVideoFeedSettingsFrameWidth.Text);
                    IPcore.VideoFeedSettings.ImageHeight = int.Parse(textBoxVideoFeedSettingsFrameHeight.Text);

                    IPcore.StopCapture();

                    // IPcore.SetResolution(IPcore.VideoFeedSettings.ImageWidth, IPcore.VideoFeedSettings.ImageHeight);
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Info, "Attempting to set resolution " + IPcore.VideoFeedSettings.ImageWidth + "x" + IPcore.VideoFeedSettings.ImageHeight));

                    IPcore.StartCaptureWithResolution(IPcore.VideoFeedSettings.ImageWidth, IPcore.VideoFeedSettings.ImageHeight);

                    (int actualFrameWidth, int actualFrameheight) = IPcore.GetResolution();
                    textBoxVideoFeedSettingsActualSize.Text = actualFrameWidth + "x" + actualFrameheight;

                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Info, "Set resolution is " + actualFrameWidth + "x" + actualFrameheight));
                }
                else Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Cannot apply settings because of invalid values"));
            }
            else Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Cannot apply settings because of invalid values"));
        }

        #endregion

        #region Capture image handling

        private void HandleVideoFeedCapture()
        {
            if (IPcore.VideoFeedSettings.IsEnabled)
            {
                string time = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + " " + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + " " + DateTime.Now.Millisecond.ToString();
                string filePath = Environment.CurrentDirectory + @"\Image Capture\" + time + ".bmp";

                using (FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    BitmapEncoder bmpEncoder = new BmpBitmapEncoder();
                    bmpEncoder.Frames.Add(BitmapFrame.Create(masterImg.Source as BitmapSource));
                    bmpEncoder.Save(fileStream);

                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Success, "Image was captured to file " + filePath));
                }
            }
        }

        #endregion

        #region Tracking Prototype

        private void TrackingPrototype(System.Drawing.Point trackingPoint, int imgHeight, int imgWidth)
        {
            // If tracking is enabled
            if (IPcore.TrackingSettings.IsEnabled == true)
            {
                // TO DO - Document and integrate in processing granulation
                // Do not track every frame => do not stress actuator
                // FrameSkipCount > 0 indicates how many frames to skip until actuator should track point
                if (++IPcore.TrackingSettings.FrameCount % IPcore.TrackingSettings.FrameSkipCount == 0)
                {
                    // Get image center point
                    System.Drawing.Point imgCenter = new System.Drawing.Point(imgWidth / 2, imgWidth / 2);

                    // For each connected actuator
                    foreach (ActuatorController actuator in Actuators.List)
                    {
                        switch (actuator.Axis)
                        {
                            // If found actuator corresponfs to axis X, track accordingly
                            case Enums.Axis.X:
                                TrackingPrototypeAxisX(trackingPoint, imgCenter, imgWidth);
                                break;

                            // If found actuator corresponfs to axis Y, track accordingly
                            case Enums.Axis.Y:
                                TrackingPrototypeAxisY(trackingPoint, imgCenter, imgHeight);
                                break;
                        }
                    }
                    // Update latest center pixel point
                    IPcore.TrackingSettings.LastestCenterPixelPoint = trackingPoint;
                }
            }
        }

        private void TrackingPrototypeAxisX(System.Drawing.Point trackingPoint, System.Drawing.Point imgCenter, int imgHeight)
        {
            // Do not call tracking method if center has not changed its position
            if (IPcore.TrackingSettings.LastestCenterPixelPoint.X != trackingPoint.X || IPcore.TrackingSettings.JustChangedAxis == true)
            {
                // Apply pixel tolerance - by what pixel distance actuator should move
                if (trackingPoint.X + IPcore.TrackingSettings.PixelTolerance <= (IPcore.TrackingSettings.LastestTrackedPixelPoint.X) ||
                    trackingPoint.X - IPcore.TrackingSettings.PixelTolerance >= (IPcore.TrackingSettings.LastestTrackedPixelPoint.X) || IPcore.TrackingSettings.JustChangedAxis == true)
                {
                    if (trackingPoint.X < imgCenter.X)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(MapRange(trackingPoint.X, 0, imgCenter.X, Apsl.MinEdgePositionStepsAllDevices, 0), 0);

                    else if (trackingPoint.X == imgCenter.X)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(0, 0);

                    else if (trackingPoint.X > imgCenter.X)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(MapRange(trackingPoint.X, imgCenter.X, imgHeight, 0, Apsl.MaxEdgePositionStepsAllDevices), 0);

                    // trackingPoint.X == int.MinValue mean that there were no contorurs found => actuator remains in last known position
                    if (trackingPoint.X != int.MinValue)
                        try
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                controller.ChangeContextByAxis(Enums.Axis.X);
                                MoveToPositionOneDeviceEventHandling(IPcore.TrackingSettings.LastestTrackedStepsPoint.X.ToString(), 0.ToString());
                            });
                        }
                        catch { /*TO DO*/ }
                }
                IPcore.TrackingSettings.JustChangedAxis = false;
            }
        }

        private void TrackingPrototypeAxisY(System.Drawing.Point trackingPoint, System.Drawing.Point imgCenter, int imgWidth)
        {
            // Do not call tracking method if center has not changed its position
            if (IPcore.TrackingSettings.LastestCenterPixelPoint.Y != trackingPoint.Y || IPcore.TrackingSettings.JustChangedAxis == true)
            {
                // Apply pixel tolerance - by what pixel distance actuator should move
                if (trackingPoint.Y + IPcore.TrackingSettings.PixelTolerance <= (IPcore.TrackingSettings.LastestTrackedPixelPoint.Y) ||
                    trackingPoint.Y - IPcore.TrackingSettings.PixelTolerance >= (IPcore.TrackingSettings.LastestTrackedPixelPoint.Y) || IPcore.TrackingSettings.JustChangedAxis == true)
                {
                    if (trackingPoint.Y < imgCenter.Y)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(0, MapRange(trackingPoint.Y, 0, imgCenter.Y, Apsl.MinEdgePositionStepsAllDevices, 0));

                    else if (trackingPoint.Y == imgCenter.Y)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(0, 0);

                    else if (trackingPoint.Y > imgCenter.Y)
                        IPcore.TrackingSettings.LastestTrackedStepsPoint = new System.Drawing.Point(0, MapRange(trackingPoint.Y, imgCenter.Y, imgWidth, 0, Apsl.MaxEdgePositionStepsAllDevices));

                    // trackingPoint.Y == int.MinValue mean that there were no contorurs found => actuator remains in last known position
                    if (trackingPoint.Y != int.MinValue)
                        try
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                controller.ChangeContextByAxis(Enums.Axis.Y);
                                MoveToPositionOneDeviceEventHandling(IPcore.TrackingSettings.LastestTrackedStepsPoint.Y.ToString(), 0.ToString());
                            });
                        }
                        catch { /*TO DO*/ }
                }
                IPcore.TrackingSettings.JustChangedAxis = false;
            }
        }

        private System.Drawing.Point MapStepsToImage()
        {
            for (int i = 0; i < Actuators.List.Count; i++)
            {
                ActuatorController actuator = Actuators.List[i];

                switch (actuator.Axis)
                {
                    case Enums.Axis.X:
                        IPcore.TrackingSettings.LastestTrackedPixelPoint = new System.Drawing.Point(MapStepsToImagePerAxis(axisXPos, IPcore.VideoFeedSettings.ImageWidth),
                                                                                             IPcore.TrackingSettings.LastestTrackedPixelPoint.Y);
                        break;

                    case Enums.Axis.Y:
                        IPcore.TrackingSettings.LastestTrackedPixelPoint = new System.Drawing.Point(IPcore.TrackingSettings.LastestTrackedPixelPoint.X,
                                                                                             MapStepsToImagePerAxis(axisYPos, IPcore.VideoFeedSettings.ImageWidth));
                        break;
                }
            }

            return IPcore.TrackingSettings.LastestTrackedPixelPoint;
        }

        private int MapStepsToImagePerAxis(int stepsPosition, int imgDimension)
        {
            if (imgDimension > 0)
            {
                if (stepsPosition < 0)
                    return MapRange(stepsPosition, Apsl.MinEdgePositionStepsAllDevices, 0, 0, imgDimension / 2);

                else if (stepsPosition > 0)
                    return MapRange(stepsPosition, 0, Apsl.MaxEdgePositionStepsAllDevices, imgDimension / 2, imgDimension);

                else return imgDimension / 2;
            }
            else return 0;
        }

        private int MapRange(float value, float from1, float to1, float from2, float to2)
        {
            return (int)((value - from1) / (to1 - from1) * (to2 - from2) + from2);
        }

        private void CalculateFeedFrameRate(Stopwatch s)
        {
            s.Stop();

            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    textBlockVideoFeedFPS.Text = "FPS: ";
                });
            }
            catch { /*TO DO*/ }
        }

        #endregion

        #region Notifications Handling

        public void NotifyUI(Enums.NotificationType notifType, string notificationText)
        {
            // Create a new row in the notification panel & add it to the grid
            RowDefinition rd = CreateNotificationRow();
            gridNotificationsCanvas.RowDefinitions.Add(rd);

            // Create and format the text for the notification
            TextBlock tb = CreateNotificationTextBlock(notificationText, notifType);

            // Assign the appropiate icon for the notification
            System.Windows.Shapes.Rectangle img = AddNotificationIcon(notifType);

            // Change the notification panel's color according to the notification type
            ColorNotificationsPanel(notifType);

            // Add the TextBlock and Rectangle (image) to the new row, as a child to the grid
            for (int colIndex = 0; colIndex < gridNotificationsCanvas.ColumnDefinitions.Count; colIndex++)
                if (colIndex == 0)
                    gridNotificationsCanvas.Children.Add(AddUIElementToNotifications(img, colIndex));
                else
                    gridNotificationsCanvas.Children.Add(AddUIElementToNotifications(tb, colIndex));

            // Increase global row index
            notificationsRowIndex++;

            // Programatically scroll down so that the user sees the latest notification
            ManageCanvasScrolling(Enums.ScrollDirection.Down);

            // TO DO - maybe
            //notifLogger.Info(notificationText);
        }

        public RowDefinition CreateNotificationRow()
        {
            int minRowHeight = 30;

            RowDefinition rd = new RowDefinition
            {
                MinHeight = minRowHeight
            };
            return rd;
        }

        public TextBlock CreateNotificationTextBlock(string notificationText, Enums.NotificationType notifType)
        {
            TextBlock tb = new TextBlock
            {
                Text = notificationText,
                FontSize = 14,
                FontFamily = new System.Windows.Media.FontFamily("Segoe UI"),
                FontWeight = FontWeights.SemiBold,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Left,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 3, 0, 3)
            };

            switch (notifType)
            {
                case Enums.NotificationType.Info:
                    tb.Foreground = uiBrushes.NotificationBlue;
                    break;

                case Enums.NotificationType.Success:
                    tb.Foreground = uiBrushes.GreenStart;
                    break;

                case Enums.NotificationType.Warning:
                    tb.Foreground = uiBrushes.NotificationYellow;
                    break;

                case Enums.NotificationType.CriticalError:
                    tb.Foreground = uiBrushes.BordeauxAccentLight;
                    break;

                case Enums.NotificationType.Camera:
                    tb.Foreground = uiBrushes.NotificationPurple;
                    break;
            }

            return tb;
        }

        public System.Windows.Shapes.Rectangle AddNotificationIcon(Enums.NotificationType notifType)
        {
            System.Windows.Shapes.Rectangle img = new System.Windows.Shapes.Rectangle
            {
                Height = 20,
                Width = 20,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            switch (notifType)
            {
                case Enums.NotificationType.Info:
                    img.Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../View/img/info.png", UriKind.Relative)) };
                    break;

                case Enums.NotificationType.Success:
                    img.Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../View/img/tick.png", UriKind.Relative)) };
                    break;

                case Enums.NotificationType.Warning:
                    img.Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../View/img/warning.png", UriKind.Relative)) };
                    break;

                case Enums.NotificationType.CriticalError:
                    img.Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../View/img/alert.png", UriKind.Relative)) };
                    break;

                case Enums.NotificationType.Camera:
                    img.Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"../../View/img/cam.png", UriKind.Relative)) };
                    break;
            }

            return img;
        }

        public UIElement AddUIElementToNotifications(UIElement uiElem, int colIndex)
        {
            Grid.SetColumn(uiElem, colIndex);
            Grid.SetRow(uiElem, notificationsRowIndex);

            return uiElem;
        }

        public void ColorNotificationsPanel(Enums.NotificationType notifType)
        {
            GradientStopCollection gsc = new GradientStopCollection(3);

            switch (notifType)
            {
                case Enums.NotificationType.Info:
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 55, 118, 162), 0));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 64, 64, 64), 0.3));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 50, 50, 50), 1));
                    //textblockNotificationHeader.Text = "Information";
                    break;

                case Enums.NotificationType.Success:
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 46, 112, 63), 0));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 64, 64, 64), 0.3));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 50, 50, 50), 1));
                    //textblockNotificationHeader.Text = "Success";
                    break;

                case Enums.NotificationType.Warning:
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 140, 136, 43), 0));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 64, 64, 64), 0.3));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 50, 50, 50), 1));
                    //textblockNotificationHeader.Text = "Warning";
                    break;

                case Enums.NotificationType.CriticalError:
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 162, 55, 55), 0));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 64, 64, 64), 0.3));
                    gsc.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 50, 50, 50), 1));
                    //textblockNotificationHeader.Text = "Critical Error";
                    break;

                    //TO DO PURPLE//
            }

            LinearGradientBrush lgb = new LinearGradientBrush(gsc, 90);
            //borderMainNotifications.Background = lgb;
        }

        public void ManageCanvasScrolling(Enums.ScrollDirection scrollDir)
        {
            this.UpdateLayout();

            double scrollAmount = gridNotificationsCanvas.ActualHeight - gridNotificationMainContainer.ActualHeight;

            if (scrollAmount > 0)
                notificationsScrollableArea.ProgrammedScrollDown(scrollAmount, scrollDir);
        }

        #endregion

        #region Typing value safety handling

        // TO DO
        // This is an ugly way to resolve the issue of textbox warning highlighting at start-up, but for now it works
        private void CheckEveryTextBoxTypingSafety()
        {
            Stopwatch s = new Stopwatch();
            s.Start();

            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsSpeedOne, Enums.TextBoxActuatorSettingsType.Speed);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsSpeedTwo, Enums.TextBoxActuatorSettingsType.Speed);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsSpeedThree, Enums.TextBoxActuatorSettingsType.Speed);

            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsMicroSpeedOne, Enums.TextBoxActuatorSettingsType.MicroSpeed);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsMicroSpeedTwo, Enums.TextBoxActuatorSettingsType.MicroSpeed);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsMicroSpeedThree, Enums.TextBoxActuatorSettingsType.MicroSpeed);

            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsAccelerationOne, Enums.TextBoxActuatorSettingsType.Acceleration);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsAccelerationTwo, Enums.TextBoxActuatorSettingsType.Acceleration);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsAccelerationThree, Enums.TextBoxActuatorSettingsType.Acceleration);

            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsDecelerationOne, Enums.TextBoxActuatorSettingsType.Deceleration);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsDecelerationTwo, Enums.TextBoxActuatorSettingsType.Deceleration);
            HandleActuatorSettingsTextBoxTypingSafety(textBoxActuatorSettingsDecelerationThree, Enums.TextBoxActuatorSettingsType.Deceleration);

            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveToPositionStepsOneDevice, textBoxMoveToPositionMicroStepsOneDevice, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveToPositionOneDevice);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxShiftToStepsOneDevice, textBoxShiftToMicroStepsOneDevice, Enums.TextBoxValueSafetyType.ShiftOn, buttonShiftOnOneDevice);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxCycleBeweenFromStepsOneDevice, textBoxCycleBeweenToStepsOneDevice, Enums.TextBoxValueSafetyType.CycleBetween, buttonCycleBetweenOneDevice);

            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceOne);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceOne);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsOne, textBoxMoveRelativelyToMicroStepsOne, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceOne);

            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceTwo);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceTwo);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsTwo, textBoxMoveRelativelyToMicroStepsTwo, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceTwo);

            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.MoveTo, buttonMoveRelativelyDeviceThree);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.ShiftOn, buttonMoveRelativelyDeviceThree);
            HandleMoveRelativelyTextBoxTypingSafety(textBoxMoveRelativelyToStepsThree, textBoxMoveRelativelyToMicroStepsThree, Enums.TextBoxValueSafetyType.CycleBetween, buttonMoveRelativelyDeviceThree);

            s.Stop();
            Console.WriteLine(s.ElapsedMilliseconds);
        }

        private void HandleMoveRelativelyTextBoxTypingSafety(TextBox textBoxOne, TextBox textBoxTwo, Enums.TextBoxValueSafetyType textBoxValueSafetyType, Button buttonToToggle)
        {
            bool methodCheck = false;
            bool tryParseValueOne = int.TryParse(textBoxOne.Text, out int valSteps);
            bool tryParseValueTwo = int.TryParse(textBoxTwo.Text, out int valMicroSteps);

            switch (textBoxValueSafetyType)
            {
                case Enums.TextBoxValueSafetyType.MoveTo:
                    methodCheck = CheckMoveToPositionOneDeviceSafetyValues(valSteps, valMicroSteps);
                    break;

                case Enums.TextBoxValueSafetyType.ShiftOn:
                    methodCheck = CheckShiftOnOneDeviceFinalPosition(valSteps, valMicroSteps);
                    break;

                case Enums.TextBoxValueSafetyType.CycleBetween:
                    methodCheck = CheckCycleBetweenOneDeviceSafetyValues(valSteps, valMicroSteps);
                    break;
            }

            if (tryParseValueOne == false || tryParseValueTwo == false || methodCheck == false)// || CheckMoveToPositionOneDeviceSafetyValues(valSteps, valMicroSteps) == false)
                ToggleMoveRelativelyControlsAtWarning(textBoxOne, textBoxTwo, Enums.TextBoxState.Warning, buttonToToggle);
            else
                ToggleMoveRelativelyControlsAtWarning(textBoxOne, textBoxTwo, Enums.TextBoxState.Default, buttonToToggle);
        }

        private void ToggleMoveRelativelyControlsAtWarning(TextBox textBoxOne, TextBox textBoxTwo, Enums.TextBoxState textBoxState, Button buttonToToggle)
        {
            SetTextBoxState(textBoxOne, textBoxState);
            SetTextBoxState(textBoxTwo, textBoxState);

            if (textBoxState == Enums.TextBoxState.Default)
                buttonToToggle.IsEnabled = true;
            else if (textBoxState == Enums.TextBoxState.Warning)
                buttonToToggle.IsEnabled = false;
        }

        private void SetTextBoxState(TextBox textBox, Enums.TextBoxState textBoxState)
        {
            switch (textBoxState)
            {
                case Enums.TextBoxState.Default:
                    textBox.Style = (Style)Application.Current.Resources["TextBoxRevealStyle"];
                    break;

                case Enums.TextBoxState.Warning:
                    textBox.Style = (Style)Application.Current.Resources["TextBoxStyleCustomWarning"];
                    break;
            }
        }

        // to do
        private void HandleActuatorSettingsTextBoxTypingSafety(TextBox textBox, Enums.TextBoxActuatorSettingsType textBoxActuatorSettingsType)
        {
            uint.TryParse(textBoxActuatorSettingsSpeedOne.Text, out uint speed);
            uint.TryParse(textBoxActuatorSettingsMicroSpeedOne.Text, out uint uspeed);
            uint.TryParse(textBoxActuatorSettingsAccelerationOne.Text, out uint accel);
            uint.TryParse(textBoxActuatorSettingsDecelerationOne.Text, out uint decel);

            move_settings_t mst = new move_settings_t
            {
                Accel = accel,
                Decel = decel,
                Speed = speed,
                uSpeed = uspeed
            };

            bool tryParseValue = int.TryParse(textBox.Text, out int value);

            if (tryParseValue == false || HandleActuatorSettingsTextBoxValueValidity(textBox, textBoxActuatorSettingsType, ref mst) == false)
                textBox.Style = (Style)Application.Current.Resources["TextBoxStyleCustomWarning"];

            else
                textBox.Style = (Style)Application.Current.Resources["TextBoxRevealStyle"];
        }

        private bool HandleActuatorSettingsTextBoxValueValidity(TextBox senderTextBox, Enums.TextBoxActuatorSettingsType textBoxActuatorSettingsType, ref move_settings_t mst)
        {
            bool methodCheck = false;

            switch (textBoxActuatorSettingsType)
            {
                case Enums.TextBoxActuatorSettingsType.Speed:
                    methodCheck = HandleActuatorSettingsTextBoxSpeed(senderTextBox, ref mst);
                    break;

                case Enums.TextBoxActuatorSettingsType.MicroSpeed:
                    methodCheck = HandleActuatorSettingsTextBoxMicroSpeed(senderTextBox, ref mst);
                    break;

                case Enums.TextBoxActuatorSettingsType.Acceleration:
                    methodCheck = HandleActuatorSettingsTextBoxAcceleration(senderTextBox, ref mst);
                    break;

                case Enums.TextBoxActuatorSettingsType.Deceleration:
                    methodCheck = HandleActuatorSettingsTextBoxDeceleration(senderTextBox, ref mst);
                    break;
            }

            return methodCheck;
        }

        // TO DO
        private bool HandleActuatorSettingsTextBoxEmpty(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (senderTextBox.Text == "")
            {
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement aborted, one or more actuator settings values are empty"));
                return false;
            }
            else return true;
        }

        // TO DO
        private bool HandleActuatorSettingsTextBoxZero(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (senderTextBox.Text == "0")
            {
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement (may be) aborted, minimum speed supported is 0 steps and 1 microsteps, acceleration and deceleration must be positive"));
                return false;
            }
            else return true;
        }

        private bool HandleActuatorSettingsTextBoxSpeed(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (HandleActuatorSettingsTextBoxEmpty(senderTextBox, ref mst) == true)
            {
                // These 3 checks are necessary because the minimum speed is 0 steps and 1 microsteps. Need to find out which textBox have teh values to be checked
                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsMicroSpeedOne, "textBoxActuatorSettingsSpeedOne") == false)
                    return false;

                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsMicroSpeedTwo, "textBoxActuatorSettingsSpeedTwo") == false)
                    return false;

                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsMicroSpeedThree, "textBoxActuatorSettingsSpeedThree") == false)
                    return false;

                if (int.Parse(senderTextBox.Text) > 1500)
                {
                    mst.Speed = 1500;
                    //textBoxActuatorSettingsSpeedOne.Text = "1500";
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Steps speed exceeds maximum value of 1500, value used will be 1500"));
                    return false;
                }
            }
            else return false;

            return true;
        }

        private bool HandleActuatorSettingsTextBoxMicroSpeed(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (HandleActuatorSettingsTextBoxEmpty(senderTextBox, ref mst) == true)
            {
                // These 3 checks are necessary because the minimul speed is 0 steps and 1 microsteps. Need to find out which textBox have teh values to be checked
                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsSpeedOne, "textBoxActuatorSettingsMicroSpeedOne") == false)
                    return false;

                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsSpeedTwo, "textBoxActuatorSettingsMicroSpeedTwo") == false)
                    return false;

                if (CheckActuatorSettingsRelatedTextBox(senderTextBox, textBoxActuatorSettingsSpeedThree, "textBoxActuatorSettingsMicroSpeedThree") == false)
                    return false;

                if (int.Parse(senderTextBox.Text) > 255)
                {
                    mst.Speed = 0;
                    //textBoxActuatorSettingsSpeedOne.Text = "1500";
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Microsteps speed exceeds maximum value of 255, value used will be 0"));
                    return false;
                }
            }
            else return false;

            return true;
        }

        private bool HandleActuatorSettingsTextBoxAcceleration(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (HandleActuatorSettingsTextBoxZero(senderTextBox, ref mst) == true && HandleActuatorSettingsTextBoxEmpty(senderTextBox, ref mst) == true)
            {
                if (int.Parse(senderTextBox.Text) > 4000)
                {
                    mst.Accel = 4000;
                    //textBoxActuatorSettingsAccelerationOne.Text = "4000";
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Acceleration value exceeds maximum of 4000, value used will be 4000"));
                    return false;
                }
                else return true;
            }
            else return false;
        }

        private bool HandleActuatorSettingsTextBoxDeceleration(TextBox senderTextBox, ref move_settings_t mst)
        {
            if (HandleActuatorSettingsTextBoxZero(senderTextBox, ref mst) == true && HandleActuatorSettingsTextBoxEmpty(senderTextBox, ref mst) == true)
            {
                if (int.Parse(senderTextBox.Text) > 4000)
                {
                    mst.Decel = 4000;
                    //textBoxActuatorSettingsDecelerationOne.Text = "4000";
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Deceleration value exceeds maximum of 4000, value used will be 4000"));
                    return false;
                }
                else return true;
            }
            else return false;
        }

        // TO DO
        private bool CheckActuatorSettingsRelatedTextBox(TextBox senderTextBox, TextBox relatedTextBox, string targetTextBoxName)
        {
            if (senderTextBox.Text != "" && relatedTextBox.Text != "")
            {
                if (int.Parse(senderTextBox.Text) == 0 && int.Parse(relatedTextBox.Text) == 0)
                    return false;
                else if (int.Parse(senderTextBox.Text) > 0)
                {
                    SetTextBoxState(senderTextBox, Enums.TextBoxState.Default);
                    SetTextBoxState(relatedTextBox, Enums.TextBoxState.Default);
                    return true;
                }
            }

            return true;
        }

        #endregion

        // TO DO
        #region Deprecated

        public bool TryConfiguringActuatorSettingsOneDevice(int listIndex = 0, TextBox sendertextBox = null)
        {
            uint speed = 0, uspeed = 0, accel = 0, decel = 0;

            // This check is necessary because the tracking algorithm uses controller.ChangeContextByAxis()
            if (IPcore.TrackingSettings.IsEnabled == false)
                controller.ChangeContext(listIndex);

            if (checkBoxActuatorSettingApplyForAll.IsChecked == true)
            {
                uint.TryParse(textBoxActuatorSettingsSpeedOne.Text, out speed);
                uint.TryParse(textBoxActuatorSettingsMicroSpeedOne.Text, out uspeed);
                uint.TryParse(textBoxActuatorSettingsAccelerationOne.Text, out accel);
                uint.TryParse(textBoxActuatorSettingsDecelerationOne.Text, out decel);
            }
            else
            {
                switch (listIndex)
                {
                    case 0:
                        uint.TryParse(textBoxActuatorSettingsSpeedOne.Text, out speed);
                        uint.TryParse(textBoxActuatorSettingsMicroSpeedOne.Text, out uspeed);
                        uint.TryParse(textBoxActuatorSettingsAccelerationOne.Text, out accel);
                        uint.TryParse(textBoxActuatorSettingsDecelerationOne.Text, out decel);
                        break;

                    case 1:
                        uint.TryParse(textBoxActuatorSettingsSpeedTwo.Text, out speed);
                        uint.TryParse(textBoxActuatorSettingsMicroSpeedTwo.Text, out uspeed);
                        uint.TryParse(textBoxActuatorSettingsAccelerationTwo.Text, out accel);
                        uint.TryParse(textBoxActuatorSettingsDecelerationTwo.Text, out decel);
                        break;

                    case 2:
                        uint.TryParse(textBoxActuatorSettingsSpeedThree.Text, out speed);
                        uint.TryParse(textBoxActuatorSettingsMicroSpeedThree.Text, out uspeed);
                        uint.TryParse(textBoxActuatorSettingsAccelerationThree.Text, out accel);
                        uint.TryParse(textBoxActuatorSettingsDecelerationThree.Text, out decel);
                        break;

                    default:
                        break;
                }
            }


            move_settings_t mst = new move_settings_t
            {
                Accel = accel,
                Decel = decel,
                Speed = speed,
                uSpeed = uspeed
            };

            bool result = false;

            if (checkBoxActuatorSettingApplyForAll.IsChecked == true)
            {
                controller.ActuatorInContext.SetMoveSettings(controller.ActuatorInContext.DeviceID, ref mst);
                return CheckActuatorSettingsOneDevice(ref mst, textBoxActuatorSettingsSpeedOne, textBoxActuatorSettingsMicroSpeedOne, textBoxActuatorSettingsAccelerationOne, textBoxActuatorSettingsDecelerationOne);
            }
            else
                switch (listIndex)
                {
                    case 0:
                        result = CheckActuatorSettingsOneDevice(ref mst, textBoxActuatorSettingsSpeedOne, textBoxActuatorSettingsMicroSpeedOne, textBoxActuatorSettingsAccelerationOne, textBoxActuatorSettingsDecelerationOne);
                        break;

                    case 1:
                        result = CheckActuatorSettingsOneDevice(ref mst, textBoxActuatorSettingsSpeedTwo, textBoxActuatorSettingsMicroSpeedTwo, textBoxActuatorSettingsAccelerationTwo, textBoxActuatorSettingsDecelerationTwo);
                        break;

                    case 2:
                        result = CheckActuatorSettingsOneDevice(ref mst, textBoxActuatorSettingsSpeedThree, textBoxActuatorSettingsMicroSpeedThree, textBoxActuatorSettingsAccelerationThree, textBoxActuatorSettingsDecelerationThree);
                        break;
                }

            if (result == true)
            {
                controller.ActuatorInContext.SetMoveSettings(controller.ActuatorInContext.DeviceID, ref mst);
                return true;
            }
            else return false;
        }

        private bool CheckActuatorSettingsOneDevice(ref move_settings_t mst, TextBox textBoxSpeed, TextBox textBoxMicroSpeed, TextBox textBoxAccel, TextBox textBoxDecel, TextBox senderTextBox = null)
        {
            if (textBoxSpeed.Text == "" ||
                textBoxMicroSpeed.Text == "" ||
                textBoxAccel.Text == "" ||
                textBoxDecel.Text == "")
            {
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement aborted, one or move actuator settings values are empty"));
                return false;
            }

            else if (textBoxSpeed.Text == "0")
            {
                if (textBoxMicroSpeed.Text == "0")
                {
                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement aborted, minimum speed supported is 0 steps and 1 microsteps"));
                    return false;
                }
            }

            else if (CheckMicroStepsValue(int.Parse(textBoxMicroSpeed.Text), Apsl.MaxPositionMicroStepsAllDevices, Apsl.MinPositionMicroStepsAllDevices) == false)
                return false;

            else if (textBoxAccel.Text == "0")
            {
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement aborted, acceleration value must be a positive integer value. Recommended value: 1000"));
                return false;
            }

            else if (textBoxDecel.Text == "0")
            {
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Movement aborted, deceleration value must be a positive integer value. Recommended value: 1000"));
                return false;
            }

            else if (int.Parse(textBoxSpeed.Text) > 1500)
            {
                mst.Speed = 1500;
                textBoxSpeed.Text = "1500";
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Steps speed exceeds maximum value of 1500, value used will be 1500"));
            }

            else if (int.Parse(textBoxAccel.Text) > 4000)
            {
                mst.Accel = 4000;
                textBoxAccel.Text = "4000";
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Acceleretaion value exceeds maximum of 4000, value used will be 4000"));
            }

            else if (int.Parse(textBoxDecel.Text) > 4000)
            {
                mst.Decel = 4000;
                textBoxDecel.Text = "4000";
                Notification.NotifList.Add(new Notification(Enums.NotificationType.Warning, "Deceleretaion value exceeds maximum of 4000, value used will be 4000"));
            }

            return true;
        }
        #endregion

        #region Move Relatively One Device handling

        private void MoveToPositionOneDeviceEventHandling(string steps, string uSteps, int MRlistIndex = 0)
        {
            if (CheckMoveToPositionOneDeviceSafetyValues(int.Parse(steps), int.Parse(uSteps)) == true)
                if (TryConfiguringActuatorSettingsOneDevice(MRlistIndex) == true) 
                    controller.ActuatorMoveToPosition(controller.ActuatorInContext.DeviceID, int.Parse(steps), int.Parse(uSteps));
        }

        private void ShiftOnPositionOneDeviceEventhandling(int deviceID, string steps, string uSteps, int MRlistIndex = 0)
        {
            if (CheckMoveToPositionOneDeviceSafetyValues(int.Parse(steps), int.Parse(uSteps)) == true)
                if (TryConfiguringActuatorSettingsOneDevice(MRlistIndex) == true)
                    controller.ActuatorMoveRelatively(controller.ActuatorInContext.DeviceID, int.Parse(steps), int.Parse(uSteps));
        }

        #endregion

        #region Move Relatively Multiple Devices Handling

        private void HandleMoveRelativelyMultipleDevices(Button targetButton, TextBox textBoxOne, TextBox textBoxTwo, int listIndex)
        {
            if (targetButton.Content.ToString() == "Move")
            {
                if (CheckMoveToPositionOneDeviceSafetyValues(int.Parse(textBoxOne.Text), int.Parse(textBoxMoveRelativelyToMicroStepsOne.Text)) == true)
                    if (TryConfiguringActuatorSettingsOneDevice(listIndex) == true)
                        controller.ActuatorMoveToPosition(Actuators.List[listIndex].DeviceID, int.Parse(textBoxOne.Text), int.Parse(textBoxTwo.Text));
            }
            else if (targetButton.Content.ToString() == "Shift")
            {
                if (CheckShiftOnOneDeviceFinalPosition(int.Parse(textBoxOne.Text), int.Parse(textBoxTwo.Text)) == true)
                    if (TryConfiguringActuatorSettingsOneDevice(listIndex) == true)
                        controller.ActuatorMoveRelatively(Actuators.List[listIndex].DeviceID, int.Parse(textBoxOne.Text), int.Parse(textBoxTwo.Text));
            }
            else if (targetButton.Content.ToString() == "Cycle")
            {
                // TO DO
            }
        }

        #endregion

        #region Move Relatively Position Calculations

        private bool CheckMicroStepsValue(int microStepsValue, int maxMicroSteps, int minMicroSteps)
        {
            if (microStepsValue > maxMicroSteps)
            {
                Console.WriteLine("Warning: Microsteps position cannot be larger than " + maxMicroSteps +
                                  ". Please set values between " + minMicroSteps + " and " + maxMicroSteps + " microsteps");
                return false;
            }

            else if (microStepsValue < minMicroSteps)
            {
                Console.WriteLine("Warning: Microsteps position cannot be smaller than " + minMicroSteps +
                                  ". Please set values between " + minMicroSteps + " and " + maxMicroSteps + " microsteps");
                return false;
            }
            else return true;
        }

        private bool CheckMoveToPositionOneDeviceSafetyValues(int stepsValue, int microStepsValue)
        {
            int maxMicroSteps = Apsl.MaxPositionMicroStepsAllDevices;
            int minMicroSteps = Apsl.MinPositionMicroStepsAllDevices;
            int maxEdgeSteps = Apsl.MaxEdgePositionStepsAllDevices;
            int minEdgeSteps = Apsl.MinEdgePositionStepsAllDevices;

            if (CheckMicroStepsValue(microStepsValue, maxMicroSteps, minMicroSteps) == false)
                return false;

            else if (stepsValue > maxEdgeSteps)
            {
                Console.WriteLine("Warning: Position " + stepsValue + " steps and " + microStepsValue +
                                  " microsteps exceeds superior safety range! Please set values between " + minEdgeSteps +
                                  " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue == maxEdgeSteps && microStepsValue > 0)
            {
                Console.WriteLine("Warning: Position " + stepsValue + " steps and " + microStepsValue +
                                  " microsteps exceeds superior safety range! Please set values between " + minEdgeSteps +
                                  " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue == minEdgeSteps && microStepsValue < 0)
            {
                Console.WriteLine("Warning: Position " + stepsValue + " steps and " + microStepsValue +
                                  " microsteps exceeds inferior safety range! Please set values between " + minEdgeSteps +
                                  " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue < minEdgeSteps)
            {
                Console.WriteLine("Warning: Position " + stepsValue + " steps and " + microStepsValue +
                                  " microsteps exceeds inferior safety range! Please set values between " + minEdgeSteps +
                                  " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }
            else return true;
        }

        private bool CheckShiftOnOneDeviceFinalPosition(int stepsValue, int microStepsValue)
        {
            int deviceID = controller.ActuatorInContext.DeviceID;

            int maxMicroSteps = Apsl.MaxPositionMicroStepsAllDevices;
            int minMicroSteps = Apsl.MinPositionMicroStepsAllDevices;
            int maxEdgeSteps = Apsl.MaxEdgePositionStepsAllDevices;
            int minEdgeSteps = Apsl.MinEdgePositionStepsAllDevices;

            int currentPositionSteps = controller.ActuatorInContext.GetCurrentPositionSteps(deviceID);
            int currentPositionMicroSteps = controller.ActuatorInContext.GetCurrentPositionMicroSteps(deviceID);

            if (CheckMicroStepsValue(microStepsValue, maxMicroSteps, minMicroSteps) == false)
                return false;

            else if (stepsValue + currentPositionSteps > maxEdgeSteps)
            {
                Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps) + " steps and " + microStepsValue +
                              " microsteps will exceed superior safety range! Please set values between " + minEdgeSteps +
                              " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue + currentPositionSteps == maxEdgeSteps && microStepsValue > 0)
            {
                Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps) + " steps and " + microStepsValue +
                             " microsteps will exceed superior safety range! Please set values between " + minEdgeSteps +
                             " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue + currentPositionSteps == (maxEdgeSteps - 1) && microStepsValue > 0)
            {
                if (microStepsValue + currentPositionMicroSteps > 256)
                {
                    Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps + 1) + " steps and " + (microStepsValue + currentPositionMicroSteps - 256) +
                             " microsteps will exceed superior safety range! Please set values between " + minEdgeSteps +
                             " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                    return false;
                }
            }

            else if (stepsValue + currentPositionSteps < minEdgeSteps)
            {
                Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps) + " steps and " + microStepsValue +
                              " microsteps will exceed inferior safety range! Please set values between " + minEdgeSteps +
                              " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue + currentPositionSteps == minEdgeSteps && microStepsValue < 0)
            {
                Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps) + " steps and " + microStepsValue +
                              " microsteps will exceed inferior safety range! Please set values between " + minEdgeSteps +
                              " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                return false;
            }

            else if (stepsValue + currentPositionSteps == (minEdgeSteps + 1) && microStepsValue < 0)
            {
                if (microStepsValue + currentPositionMicroSteps < -256)
                {
                    Console.WriteLine("Warning: Position " + (stepsValue + currentPositionSteps - 1) + " steps and " + (microStepsValue + currentPositionMicroSteps + 256) +
                             " microsteps will exceed inferior safety range! Please set values between " + minEdgeSteps +
                             " steps, 0 μsteps and " + maxEdgeSteps + " steps, 0 μsteps");
                    return false;
                }
            }

            return true;
        }

        private bool CheckCycleBetweenOneDeviceSafetyValues(int stepsFromValue, int stepsToValue)
        {
            int maxEdgeSteps = Apsl.MaxEdgePositionStepsAllDevices;
            int minEdgeSteps = Apsl.MinEdgePositionStepsAllDevices;

            if (stepsFromValue > maxEdgeSteps)
            {
                Console.WriteLine("Cycle warning: Position " + stepsFromValue + " steps exceeds superior safety range! Please set values between " +
                                  minEdgeSteps + " steps and " + maxEdgeSteps + " steps");
                return false;
            }

            else if (stepsFromValue < minEdgeSteps)
            {
                Console.WriteLine("Cycle warning: Position " + stepsFromValue + " steps inferior inferior safety range! Please set values between " +
                                  minEdgeSteps + " steps and " + maxEdgeSteps + " steps");
                return false;
            }

            else if (stepsToValue > maxEdgeSteps)
            {
                Console.WriteLine("Cycle warning: Position " + stepsToValue + " steps exceeds superior safety range! Please set values between " +
                                  minEdgeSteps + " steps and " + maxEdgeSteps + " steps");
                return false;
            }

            else if (stepsToValue < minEdgeSteps)
            {
                Console.WriteLine("Cycle warning: Position " + stepsToValue + " steps exceeds inferior safety range! Please set values between " +
                                  minEdgeSteps + " steps and " + maxEdgeSteps + " steps");
                return false;
            }
            else return true;
        }

        #endregion

        #region Connection Settings Swap Handling

        private void ComboBoxConnectionSettingsHandleSwap()
        {
            try
            {
                int[] newIndexValues = new int[3];

                newIndexValues[0] = comboBoxConnectionSettingsDeviceOne.SelectedIndex;
                newIndexValues[1] = comboBoxConnectionSettingsDeviceTwo.SelectedIndex;
                newIndexValues[2] = comboBoxConnectionSettingsDeviceThree.SelectedIndex;

                if (comboBoxOneIndex != -1 && comboBoxTwoIndex != -1 && comboBoxThreeIndex != -1)
                    if (newIndexValues[0] != comboBoxOneIndex)
                        GetDifferentConnectionSettingsComboBoxIndex(newIndexValues[0],
                                                comboBoxConnectionSettingsDeviceTwo,
                                                comboBoxConnectionSettingsDeviceThree).SelectedIndex = comboBoxOneIndex;

                    else if (newIndexValues[1] != comboBoxTwoIndex)
                        GetDifferentConnectionSettingsComboBoxIndex(newIndexValues[1],
                                                comboBoxConnectionSettingsDeviceOne,
                                                comboBoxConnectionSettingsDeviceThree).SelectedIndex = comboBoxTwoIndex;

                    else if (newIndexValues[2] != comboBoxThreeIndex)
                        GetDifferentConnectionSettingsComboBoxIndex(newIndexValues[2],
                                                                    comboBoxConnectionSettingsDeviceOne,
                                                                    comboBoxConnectionSettingsDeviceTwo).SelectedIndex = comboBoxThreeIndex;

                // TO DO - comment
                // Do not change order - Subsequent calls are dependent on the previous one to finish
                GetConnectionSettingsComboBoxSelectedItemIndex();
                SetActuatorAxis();
                ArrangeMoveContinuouslyPanel(deviceCount);
                SetLabelsText();
                SetSliderValueBindings();
                IPcore.TASettings.SetConnectedAxis(controller.Actuators);
                IPcore.TrackingSettings.JustChangedAxis = true;

                // TO DO - delete
                Console.WriteLine(comboBoxConnectionSettingsDeviceOne.SelectedIndex);
                Console.WriteLine(comboBoxConnectionSettingsDeviceTwo.SelectedIndex);
                Console.WriteLine(comboBoxConnectionSettingsDeviceThree.SelectedIndex);
            }
            catch
            { /* TO DO */
            }
        }

        private ComboBox GetDifferentConnectionSettingsComboBoxIndex(int newIndexValue, ComboBox cb1, ComboBox cb2)
        {
            if (cb1.SelectedIndex == newIndexValue)
                return cb1;
            else if (cb2.SelectedIndex == newIndexValue)
                return cb2;

            return null;
        }

        private void GetConnectionSettingsComboBoxSelectedItemIndex()
        {
            comboBoxOneIndex = comboBoxConnectionSettingsDeviceOne.SelectedIndex;
            comboBoxTwoIndex = comboBoxConnectionSettingsDeviceTwo.SelectedIndex;
            comboBoxThreeIndex = comboBoxConnectionSettingsDeviceThree.SelectedIndex;
        }

        private void SetActuatorAxis()
        {
            // Loop through each connected actuator
            for (int i = 0; i < Actuators.List.Count; i++)
            {
                // Dictionary tranlating comboBox index to Axis (X, Y, or Z) enum
                Dictionary<int, Enums.Axis> dictionary = new Dictionary<int, Enums.Axis>()
                {
                    {0, Enums.Axis.X}, {1, Enums.Axis.Y}, {2, Enums.Axis.Z}
                };

                try
                {
                    // Get actuator
                    ActuatorController currentActuator = Actuators.List[i];

                    // Set actuator axis based on index of comboBox
                    if (i == 0)
                        currentActuator.SetAxis(currentActuator.DeviceID, dictionary[comboBoxOneIndex]);
                    else if (i == 1)
                        currentActuator.SetAxis(currentActuator.DeviceID, dictionary[comboBoxTwoIndex]);
                    else if (i == 2)
                        currentActuator.SetAxis(currentActuator.DeviceID, dictionary[comboBoxThreeIndex]);


                    Notification.NotifList.Add(new Notification(Enums.NotificationType.Info, "Actuator with ID = " + currentActuator.DeviceID + " and name " + currentActuator.UIName + " got connected as axis " + currentActuator.GetAxis(currentActuator.DeviceID).ToString()));
                }
                // TO DO - log
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        #endregion

        #region Move Relatively button and label change

        private void HandleMoveRelativelyButtonTextChange(ComboBox senderComboBox, Button targetButton)
        {
            string[] parts = senderComboBox.SelectedItem.ToString().Split(' ');

            if (targetButton != null)
                targetButton.Content = parts[1];
        }

        private void HandleCycleBetweenTextBlockChange(ComboBox senderComboBox, TextBlock targetTextBlock)
        {
            string[] parts = senderComboBox.SelectedItem.ToString().Split(' ');

            if (parts[1] == "Cycle")
                targetTextBlock.Text = "  steps";
            else if (targetTextBlock != null)
                targetTextBlock.Text = "μsteps";
        }

        #endregion

        #region Text Box Value Validity

        private void CheckSignedTextboxValidityAtPreviewInput(TextCompositionEventArgs e)
        {
            // Get characters from the text which is to be pasted
            char[] chars = (e.Source as TextBox).Text.ToCharArray();

            // Get caret (cursor) position inside the textBox
            int caretPosition = (e.Source as TextBox).SelectionStart;

            // Get selection lenght (highlighted text) inside textBox, if any
            int selectionLength = (e.Source as TextBox).SelectionLength;

            // Create a string containing the text from the textBox before the caret's position
            string preSelectionString = (e.Source as TextBox).Text.Substring(0, caretPosition);

            // Create a string containing the text from the textBox after the caret's position
            string postSelectionString = (e.Source as TextBox).Text.Substring(caretPosition + selectionLength, (e.Source as TextBox).Text.Length - (caretPosition + selectionLength));

            // Create a string that would represent the textBox's text after it would pe pasted
            string newTextBoxString = preSelectionString + e.Text + postSelectionString;

            // TO DO
            // Text cannot have 0 value, or start with a 0
            //if (newTextBoxString.Length >= 0 && newTextBoxString[0] == '0')
            // Text is not in correct format, cancel further processing
            // e.Handled = true;

            // Text cannot start with "-0"
            if (newTextBoxString.Length > 1 && newTextBoxString[0] == '-' && newTextBoxString[1] == '0')
                // Text is not in correct format, cancel further processing
                e.Handled = true;

            //  If there are illegal characters inside the new text
            else if (_signedRegex.IsMatch(newTextBoxString) == true)
                // Text is not in correct format, cancel further processing
                e.Handled = true;

            // If the lenght of the new text is larger than the maximum allowed 6 characters
            else if (newTextBoxString.Length > 6)
                // Text is not in correct format, cancel further processing
                e.Handled = true;

            else
                for (int i = 1; i < newTextBoxString.Length; i++)
                    // If any characters after the first character are "-" signs
                    if ((int)newTextBoxString[i] == 45)
                        // Text is not in correct format, cancel further processing
                        e.Handled = true;
        }

        private bool CheckSignedTextboxValidityAtPaste(string text, DataObjectPastingEventArgs e)
        {
            // Get characters from the text which is to be pasted
            char[] chars = text.ToCharArray();

            // If there are any characters to be pasted
            if (chars.Length > 0)
            {
                // If there are any caracters in the textBox in focus
                if ((e.Source as TextBox).Text.Length > 0)
                {
                    // Get caret (cursor) position inside the textBox
                    int caretPosition = (e.Source as TextBox).SelectionStart;

                    // Get selection lenght (highlighted text) inside textBox, if any
                    int selectionLength = (e.Source as TextBox).SelectionLength;

                    // Create a string containing the text from the textBox before the caret's position
                    string preSelectionString = (e.Source as TextBox).Text.Substring(0, caretPosition);

                    // Create a string containing the text from the textBox after the caret's position
                    string postSelectionString = (e.Source as TextBox).Text.Substring(caretPosition + selectionLength, (e.Source as TextBox).Text.Length - (caretPosition + selectionLength));

                    // Create a string that would represent the textBox's text after it would pe pasted
                    string newTextBoxString = preSelectionString + text + postSelectionString;

                    //  If there are illegal characters inside the new text
                    if (_signedRegex.IsMatch(newTextBoxString) == true)
                        // Text is not in correct format, return false
                        return false;

                    // If the lenght of the new text is larger than the maximum allowed 6 characters
                    else if (newTextBoxString.Length > 6)
                        // Text is not in correct format, return false
                        return false;

                    else
                        for (int i = 1; i < newTextBoxString.Length; i++)
                            // If any characters after the first character are "-" signs
                            if ((int)newTextBoxString[i] == 45)
                                // Text is not in correct format, return false
                                return false;
                }
            }
            // New text is in correct format, return true
            return true;
        }

        #endregion

        #region Application Configuration Manager methods

        public List<string> GetConnectionSettingsComboBoxSelectedIndex()
        {
            List<string> axisString = GetConnectionSettingsSelectedText();
            List<string> selectedIndexList = new List<string>();

            foreach (string axis in axisString)
                switch (axis)
                {
                    case "X":
                        selectedIndexList.Add("0");
                        break;

                    case "Y":
                        selectedIndexList.Add("1");
                        break;

                    case "Z":
                        selectedIndexList.Add("2");
                        break;

                    default:
                        break;
                }

            return selectedIndexList;
        }

        public List<string> GetMoveRelativelyMultipleDevicesComboBoxSelectedIndex()
        {
            List<string> selectedIndexList = new List<string>()
            {
                comboBoxMoveRelativelyDeviceOne.SelectedIndex.ToString(),
                comboBoxMoveRelativelyDeviceTwo.SelectedIndex.ToString(),
                comboBoxMoveRelativelyDeviceThree.SelectedIndex.ToString()
            };

            return selectedIndexList;
        }

        #endregion

        #region Set Labels Text

        private void SetLabelsText()
        {
            List<string> deviceNames = new List<string>();

            for (int i = 0; i < deviceCount; i++)
            {
                deviceNames.Add(Actuators.List[i].UIName);

                switch (i)
                {
                    case 0:
                        SetLabelsTextDeviceOne(deviceNames[i]);
                        break;

                    case 1:
                        SetLabelsTextDeviceTwo(deviceNames[i]);
                        break;

                    case 2:
                        SetLabelsTextDeviceThree(deviceNames[i]);
                        break;
                }
            }

            SetSliderAxisLabels(deviceNames);
        }

        private void SetLabelsTextDeviceOne(string deviceName)
        {
            textBlockctuatorSettingsDeviceOneLabel.Text = deviceName;
            textBlockConnectionSettingsDeviceOne.Text = deviceName + " to axis: ";
            textBlockMoveRelativelyDeviceOneLabel.Text = deviceName;
            textBlockActuatorStatusDeviceOneLabel.Text = deviceName;
            textBlockPowerStatusDeviceOneLabel.Text = deviceName;
        }

        private void SetLabelsTextDeviceTwo(string deviceName)
        {
            textBlockctuatorSettingsDeviceTwoLabel.Text = deviceName;
            textBlockConnectionSettingsDeviceTwo.Text = deviceName + " to axis: ";
            textBlockMoveRelativelyDeviceTwoLabel.Text = deviceName;
            textBlockActuatorStatusDeviceTwoLabel.Text = deviceName;
            textBlockPowerStatusDeviceTwoLabel.Text = deviceName;
        }

        private void SetLabelsTextDeviceThree(string deviceName)
        {
            textBlockctuatorSettingsDeviceThreeLabel.Text = deviceName;
            textBlockConnectionSettingsDeviceThree.Text = deviceName + " to axis: ";
            textBlockMoveRelativelyDeviceThreeLabel.Text = deviceName;
            textBlockActuatorStatusDeviceThreeLabel.Text = deviceName;
            textBlockPowerStatusDeviceThreeLabel.Text = deviceName;
        }

        private void SetSliderAxisLabels(List<string> deviceNames)
        {
            List<string> axisString = GetConnectionSettingsSelectedText();

            for (int i = 0; i < deviceNames.Count; i++)
                GetSliderLabelForSpecificAxis(axisString[i]).Text = "Axis " + axisString[i] + " (" + deviceNames[i] + ")";
        }

        public List<string> GetConnectionSettingsSelectedText()
        {
            string[] comboBoxOne = comboBoxConnectionSettingsDeviceOne.SelectedItem.ToString().Split(' ');
            string[] comboBoxTwo = comboBoxConnectionSettingsDeviceTwo.SelectedItem.ToString().Split(' ');
            string[] comboBoxThree = comboBoxConnectionSettingsDeviceThree.SelectedItem.ToString().Split(' ');

            List<string> axisString = new List<string>()
            {
                comboBoxOne[1],
                comboBoxTwo[1],
                comboBoxThree[1]
            };

            return axisString;
        }

        private TextBlock GetSliderLabelForSpecificAxis(string axis)
        {
            switch (axis)
            {
                case "X":
                    return textBlockMoveContinuouslyAxisX;

                case "Y":
                    return textBlockMoveContinuouslyAxisY;

                case "Z":
                    return textBlockMoveContinuouslyAxisZ;

                default:
                    return null;
            }
        }

        #endregion

        #region Slider Bindings

        private void SetSliderValueBindings()
        {
            List<string> axisString = GetConnectionSettingsSelectedText();

            for (int i = 0; i < axisString.Count; i++)
                switch (i)
                {
                    case 0:
                        CreateSliderValueBinding(GetSliderForSpecificAxis(axisString[i]), textBlockActuatorStatusPositionDeviceOne);
                        break;

                    case 1:
                        CreateSliderValueBinding(GetSliderForSpecificAxis(axisString[i]), textBlockActuatorStatusPositionDeviceTwo);
                        break;

                    case 2:
                        CreateSliderValueBinding(GetSliderForSpecificAxis(axisString[i]), textBlockActuatorStatusPositionDeviceThree);
                        break;
                }

            SetSliderEdgeValues();
        }

        private Slider GetSliderForSpecificAxis(string axis)
        {
            switch (axis)
            {
                case "X":
                    return sliderMoveContinuouslyAxisX;

                case "Y":
                    return sliderMoveContinuouslyAxisY;

                case "Z":
                    return sliderMoveContinuouslyAxisZ;

                default:
                    return null;
            }
        }

        private void CreateSliderValueBinding(Slider targetSlider, TextBlock targetTextBlock)
        {
            Binding newBinding = new Binding("Text")
            {
                Source = targetTextBlock
            };

            targetSlider.SetBinding(Slider.ValueProperty, newBinding);
        }

        private void SetSliderEdgeValues()
        {
            sliderMoveContinuouslyAxisX.Minimum =
                sliderMoveContinuouslyAxisY.Minimum =
                sliderMoveContinuouslyAxisZ.Minimum =
                Apsl.MinEdgePositionStepsAllDevices;

            sliderMoveContinuouslyAxisX.Maximum =
                sliderMoveContinuouslyAxisY.Maximum =
                sliderMoveContinuouslyAxisZ.Maximum =
                Apsl.MaxEdgePositionStepsAllDevices;
        }

        #endregion

        #region Arrange Components Main

        private void ArrangeComponents(int deviceCount)
        {
            ArrangeActuatorSettingsPanel(deviceCount);
            ArrangeConnectionSettingsPanel(deviceCount);
            ArrangeMoveRelativelyPanel(deviceCount);
            ArrangeMoveContinuouslyPanel(deviceCount);
            ArrangeActuatorStatusPanel(deviceCount);
            ArrangePowerStatusPanel(deviceCount);
            SetAllowForAllCheckBoxesVisibility();
        }

        private void SetAllowForAllCheckBoxesVisibility()
        
        {
            if (controller != null && controller.Actuators.Count == 1)
            {
                checkBoxActuatorSettingApplyForAll.Visibility = Visibility.Hidden;
                checkBoxMoveRelativelyApplyForAll.Visibility = Visibility.Hidden;
                rowDefinitionActuatorSettingsCheckBox.Height = new GridLength(10);
                rowDefinitionMoveRelativelyCheckBox.Height = new GridLength(0);
            }
            else if (controller != null && controller.Actuators.Count > 1)
            {
                checkBoxActuatorSettingApplyForAll.Visibility = Visibility.Visible;
                checkBoxMoveRelativelyApplyForAll.Visibility = Visibility.Visible;
                rowDefinitionActuatorSettingsCheckBox.Height = new GridLength(35);
                rowDefinitionMoveRelativelyCheckBox.Height = new GridLength(35);
            }
        }

        #endregion

        #region Arrange Actuator Settings Panel UI

        // TO DO - virtual mode
        private void ArrangeActuatorSettingsPanel(int deviceCount)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                if (virtualMode == true)
                    ChangeActuatorSettingsDeviceCountUI(int.Parse(parts[1]));
                else
                    ChangeActuatorSettingsDeviceCountUI(deviceCount);
            }
            catch { }
        }

        private void ChangeActuatorSettingsDeviceCountUI(int newAmount)
        {
            if (newAmount == 1)
            {
                columnDefinitionSettingsColumn.Width = new GridLength(250);
                //textBlockActuatorSettingsLabelDeviceOne.Text = "All";
                columnDefinitionActuatorSettingsLabelsColumn.Width = new GridLength(120);
                columnDefinitionActuatorSettingsDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorSettingsDeviceTwoColumn.Width = new GridLength(0);
                columnDefinitionActuatorSettingsDeviceThreeColumn.Width = new GridLength(0);
            }
            else if (newAmount == 2)
            {
                columnDefinitionSettingsColumn.Width = new GridLength(310);

                columnDefinitionActuatorSettingsLabelsColumn.Width = new GridLength(120);
                columnDefinitionActuatorSettingsDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorSettingsDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorSettingsDeviceThreeColumn.Width = new GridLength(0);
            }
            else if (newAmount == 3)
            {
                columnDefinitionSettingsColumn.Width = new GridLength(310);

                columnDefinitionActuatorSettingsLabelsColumn.Width = new GridLength(100);
                columnDefinitionActuatorSettingsDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorSettingsDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorSettingsDeviceThreeColumn.Width = new GridLength(1, GridUnitType.Star);
            }
        }

        #endregion

        #region Arrange Connection Settings Panel UI

        // TO DO - virtual mode
        private void ArrangeConnectionSettingsPanel(int deviceCount)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                if (virtualMode == true)
                    ChangeConnectionSettingsDeviceCountUI(int.Parse(parts[1]));
                else
                    ChangeConnectionSettingsDeviceCountUI(deviceCount);
            }
            catch { }
        }

        private void ChangeConnectionSettingsDeviceCountUI(int newAmount)
        {
            if (newAmount == 1)
            {
                rowDefinitionConnectionSettingsDeviceOne.Height = new GridLength(1, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceTwo.Height = new GridLength(0, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceThree.Height = new GridLength(0, GridUnitType.Star);
            }
            else if (newAmount == 2)
            {
                rowDefinitionConnectionSettingsDeviceOne.Height = new GridLength(1, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceTwo.Height = new GridLength(1, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceThree.Height = new GridLength(0, GridUnitType.Star);
            }
            else if (newAmount == 3)
            {
                rowDefinitionConnectionSettingsDeviceOne.Height = new GridLength(1, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceTwo.Height = new GridLength(1, GridUnitType.Star);
                rowDefinitionConnectionSettingsDeviceThree.Height = new GridLength(1, GridUnitType.Star);
            }
        }

        #endregion

        #region Arrange Move Relatively Panel UI

        // TO DO - virtual mode
        private void ArrangeMoveRelativelyPanel(int deviceCount)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                if (virtualMode == true)
                    ChangeMoveRelativelyDeviceCountUI(int.Parse(parts[1]));
                else { }
                // TO DO - CE FACE CHESTIA ASTA?
                ChangeMoveRelativelyDeviceCountUI(deviceCount);
            }
            catch { }
        }

        private void ChangeMoveRelativelyDeviceCountUI(int newAmount)
        {
            rowDefinitionMoveRelativelyRowDeviceThree.Height = new GridLength(1, GridUnitType.Star);

            if (newAmount == 1)
            {
                HideGrid(gridMoveRelativelyFirstRowMultipleDevices);
                HideGrid(gridMoveRelativelySecondRowMultipleDevices);
                HideGrid(gridMoveRelativelyThirdRowMultipleDevices);

                ShowGrid(gridMoveRelativelyFirstRowOneDevice);
                ShowGrid(gridMoveRelativelySecondRowOneDevice);
                ShowGrid(gridMoveRelativelyThirdRowOneDevice);
            }
            else if (newAmount == 2)
            {
                HideGrid(gridMoveRelativelyFirstRowOneDevice);
                HideGrid(gridMoveRelativelySecondRowOneDevice);
                HideGrid(gridMoveRelativelyThirdRowOneDevice);

                ShowGrid(gridMoveRelativelyFirstRowMultipleDevices);
                ShowGrid(gridMoveRelativelySecondRowMultipleDevices);

                rowDefinitionMoveRelativelyRowDeviceThree.Height = new GridLength(0, GridUnitType.Star);
            }
            else if (newAmount == 3)
            {
                HideGrid(gridMoveRelativelyFirstRowOneDevice);
                HideGrid(gridMoveRelativelySecondRowOneDevice);
                HideGrid(gridMoveRelativelyThirdRowOneDevice);

                ShowGrid(gridMoveRelativelyFirstRowMultipleDevices);
                ShowGrid(gridMoveRelativelySecondRowMultipleDevices);
                ShowGrid(gridMoveRelativelyThirdRowMultipleDevices);
            }
        }

        #endregion

        #region Arrange Move Continuously Panel UI

        // TO DO - virtual mode
        private void ArrangeMoveContinuouslyPanel(int deviceCount)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                if (virtualMode == true)
                    ChangeMoveContinuouslyDeviceCountUI(int.Parse(parts[1]));
                else
                    ChangeMoveContinuouslyDeviceCountUI(deviceCount);
            }
            catch { }
        }

        public void ChangeMoveContinuouslyDeviceCountUI(int deviceCount)
        {
            try
            {
                ResetMoveContinuouslyPanel();

                if (deviceCount.ToString() == "1")
                {
                    string[] partsOne = comboBoxConnectionSettingsDeviceOne.SelectedItem.ToString().Split(' ');

                    gridMoveRelativelyButtonsColumnOne.Visibility = Visibility.Hidden;
                    gridMoveRelativelyButtonsColumnThree.Visibility = Visibility.Hidden;

                    rowDefinitionMoveContinuouslyPanel.Height = new GridLength(200);

                    switch (partsOne[1])
                    {
                        case "X":
                            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisX(controller, Apsl, this));
                            break;

                        case "Y":
                            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisY(controller, Apsl, this));
                            break;

                        case "Z":
                            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisZ(controller, Apsl, this));
                            break;
                    }
                }
                else if (deviceCount.ToString() == "2")
                {
                    string[] partsOne = comboBoxConnectionSettingsDeviceOne.SelectedItem.ToString().Split(' ');
                    string[] partsTwo = comboBoxConnectionSettingsDeviceTwo.SelectedItem.ToString().Split(' ');

                    columnDefinitionMoveRelativelyButtonsColumnOne.Width = new GridLength(1, GridUnitType.Star);
                    columnDefinitionMoveRelativelyButtonsColumnTwo.Width = new GridLength(1, GridUnitType.Star);
                    columnDefinitionMoveRelativelyButtonsColumnThree.Width = new GridLength(0, GridUnitType.Star);

                    gridMoveRelativelyButtonsColumnThree.Visibility = Visibility.Hidden;

                    switch (partsOne[1])
                    {
                        case "X":
                            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnOne.Children.Clear();
                            gridMoveRelativelyButtonsColumnOne.Children.Add(new ActuatorMoveContinuouslyAxisX(controller, Apsl, this));
                            break;

                        case "Y":
                            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnOne.Children.Clear();
                            gridMoveRelativelyButtonsColumnOne.Children.Add(new ActuatorMoveContinuouslyAxisY(controller, Apsl, this));
                            break;

                        case "Z":
                            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnOne.Children.Clear();
                            gridMoveRelativelyButtonsColumnOne.Children.Add(new ActuatorMoveContinuouslyAxisZ(controller, Apsl, this));
                            break;
                    }

                    switch (partsTwo[1])
                    {
                        case "X":
                            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisX(controller, Apsl, this));
                            break;

                        case "Y":
                            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisY(controller, Apsl, this));
                            break;

                        case "Z":
                            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Visible;

                            gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                            gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisZ(controller, Apsl, this));
                            break;
                    }
                }
                else if (deviceCount.ToString() == "3")
                {
                    gridMoveRelativelyButtonsColumnOne.Children.Clear();
                    gridMoveRelativelyButtonsColumnTwo.Children.Clear();
                    gridMoveRelativelyButtonsColumnThree.Children.Clear();

                    gridMoveRelativelyButtonsColumnOne.Children.Add(new ActuatorMoveContinuouslyAxisZ(controller, Apsl, this));
                    gridMoveRelativelyButtonsColumnTwo.Children.Add(new ActuatorMoveContinuouslyAxisX(controller, Apsl, this));
                    gridMoveRelativelyButtonsColumnThree.Children.Add(new ActuatorMoveContinuouslyAxisY(controller, Apsl, this));

                    EnableAllSliders();
                }

                if (IsHiddenSliderAxisX() == true)
                    rowDefinitionMoveContinuouslyLowerSlider.Height = new GridLength(0);
            }
            catch
            {

            }
        }
        private void ResetMoveContinuouslyPanel()
        {
            columnDefinitionMoveRelativelyButtonsColumnOne.Width = new GridLength(1, GridUnitType.Star);
            columnDefinitionMoveRelativelyButtonsColumnTwo.Width = new GridLength(1.5, GridUnitType.Star);
            columnDefinitionMoveRelativelyButtonsColumnThree.Width = new GridLength(1, GridUnitType.Star);

            gridMoveRelativelyButtonsColumnOne.Visibility = Visibility.Visible;
            gridMoveRelativelyButtonsColumnTwo.Visibility = Visibility.Visible;
            gridMoveRelativelyButtonsColumnThree.Visibility = Visibility.Visible;

            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Visible;
            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Visible;
            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Visible;

            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Hidden;
            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Hidden;
            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Hidden;

            rowDefinitionMoveContinuouslyLowerSlider.Height = new GridLength(60);
            rowDefinitionMoveContinuouslyPanel.Height = new GridLength(250);
        }

        private void EnableAllSliders()
        {
            gridMoveContinuouslySliderAxisX.Visibility = Visibility.Visible;
            gridMoveContinuouslySliderAxisY.Visibility = Visibility.Visible;
            gridMoveContinuouslySliderAxisZ.Visibility = Visibility.Visible;
        }

        private bool IsHiddenSliderAxisX()
        {
            if (gridMoveContinuouslySliderAxisX.Visibility == Visibility.Hidden)
                return true;
            else return false;
        }

        #endregion

        #region Arrange Actuator Status Panel UI

        // TO DO - virtual mode
        private void ArrangeActuatorStatusPanel(int deviceID)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                if (virtualMode == true)
                    ChangeActuatorStatusDeviceCountUI(int.Parse(parts[1]));
                else
                    ChangeActuatorStatusDeviceCountUI(deviceCount);
            }
            catch { }
        }

        private void ChangeActuatorStatusDeviceCountUI(int newAmount)
        {
            if (newAmount == 1)
            {
                //textBlockActuatorSettingsLabelDeviceOne.Text = "All";
                columnDefinitionActuatorStatusLabelsColumn.Width = new GridLength(100);
                columnDefinitionActuatorStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorStatusDeviceTwoColumn.Width = new GridLength(0);
                columnDefinitionActuatorStatusDeviceThreeColumn.Width = new GridLength(0);

                columnDefinitionStatusColumn.Width = new GridLength(250);
            }
            else if (newAmount == 2)
            {
                columnDefinitionActuatorStatusLabelsColumn.Width = new GridLength(120);
                columnDefinitionActuatorStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorStatusDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorStatusDeviceThreeColumn.Width = new GridLength(0);

                columnDefinitionStatusColumn.Width = new GridLength(300);
            }
            else if (newAmount == 3)
            {
                columnDefinitionActuatorStatusLabelsColumn.Width = new GridLength(100);
                columnDefinitionActuatorStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorStatusDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionActuatorStatusDeviceThreeColumn.Width = new GridLength(1, GridUnitType.Star);

                columnDefinitionStatusColumn.Width = new GridLength(300);
            }
        }

        #endregion

        #region Arrange Power Status Panel UI

        // TO DO - virtual mode
        private void ArrangePowerStatusPanel(int deviceCount)
        {
            try
            {
                string[] parts = comboBoxTest.SelectedItem.ToString().Split(' ');

                ChangePowerStatusDeviceCountUI(int.Parse(parts[1]));

                if (virtualMode == true)
                    ChangePowerStatusDeviceCountUI(int.Parse(parts[1]));
                else
                    ChangePowerStatusDeviceCountUI(deviceCount);
            }
            catch { }
        }
        private void ChangePowerStatusDeviceCountUI(int newAmount)
        {
            if (newAmount == 1)
            {
                //textBlockActuatorSettingsLabelDeviceOne.Text = "All";
                columnDefinitionPowerStatusLabelsColumn.Width = new GridLength(100);
                columnDefinitionPowerStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionPowerStatusDeviceTwoColumn.Width = new GridLength(0);
                columnDefinitionPowerStatusDeviceThreeColumn.Width = new GridLength(0);
            }
            else if (newAmount == 2)
            {
                columnDefinitionPowerStatusLabelsColumn.Width = new GridLength(120);
                columnDefinitionPowerStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionPowerStatusDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionPowerStatusDeviceThreeColumn.Width = new GridLength(0);
            }
            else if (newAmount == 3)
            {
                columnDefinitionPowerStatusLabelsColumn.Width = new GridLength(100);
                columnDefinitionPowerStatusDeviceOneColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionPowerStatusDeviceTwoColumn.Width = new GridLength(1, GridUnitType.Star);
                columnDefinitionPowerStatusDeviceThreeColumn.Width = new GridLength(1, GridUnitType.Star);
            }
        }

        #endregion

        // TO DO
        ////////
        #region UNSORTED

        private void HideGrid(Grid targetGrid)
        {
            targetGrid.Visibility = Visibility.Hidden;
        }

        private void ShowGrid(Grid targetGrid)
        {
            targetGrid.Visibility = Visibility.Visible;
        }

        #endregion
        ////////

        #endregion Methods

        // TO DO
        #region Unused / Unsafe / Deprecated / Debugging

        private void ButtonSaveCFG_Click(object sender, RoutedEventArgs e)
        {
            AppConfigManager.SaveAllToConfiguration();
        }

        private void ButtonLoadCFG_Click(object sender, RoutedEventArgs e)
        {
            AppConfigManager.LoadAllFromConfiguration();
        }

        // To be deleted
        private void CheckBoxVirtualMode_Click(object sender, RoutedEventArgs e)
        {
            if (checkBoxVirtualMode.IsChecked == true)
                virtualMode = true;
            else
                virtualMode = false;
        }

        // This is very unsafe. Almost locked the engine on on edge. DO NOT USE
        private void ButtonConnectionSettingsCalibrateDeviceOne_Click(object sender, RoutedEventArgs e)
        {
            //List<int> sliderEdgeValues = controller.ActuatorCalibrateMovement(controller.ActuatorInContext.DeviceID);
            //foreach (int edgeValue in sliderEdgeValues)
            //   Console.WriteLine(edgeValue);
        }

        // This is very unsafe. Almost locked the engine on on edge. DO NOT USE
        private void ButtonConnectionSettingsCalibrateDeviceTwo_Click(object sender, RoutedEventArgs e)
        {
            // unsafe
        }

        // This is very unsafe. Almost locked the engine on on edge. DO NOT USE
        private void ButtonConnectionSettingsCalibrateDeviceThree_Click(object sender, RoutedEventArgs e)
        {
            // unsafe
        }

        #endregion

    }
}