﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;

namespace View.UserControls
{
    /// <summary>
    /// Interaction logic for ActuatorSettingsPanel.xaml
    /// </summary>
    public partial class ActuatorSettingsPanel : UserControl
    {
        ActuatorController actuator;

        public ActuatorSettingsPanel(ActuatorController actuator)
        {
            InitializeComponent();

            this.actuator = actuator;
        }
    }
}
