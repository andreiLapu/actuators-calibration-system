﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Versioning;
using System.Configuration;
using View.UserControls;


namespace View
{
    public class ApplicationConfigurationManager
    {
        MainControlsPanel mcp;
        Configuration configuration;

        public ApplicationConfigurationManager(MainControlsPanel mcp)
        {
            this.mcp = mcp;
            ConfigureSettings();
        }

        private void ConfigureSettings()
        {
            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string configFile = System.IO.Path.Combine(appPath, "App.config");
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFile;
            //configuration = ConfigurationManager.Open(configFileMap, ConfigurationUserLevel.None);
            configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        public void SaveAllToConfiguration()
        {
            try
            {
                // Actuator Settings panel
                configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedOne"].Value = mcp.textBoxActuatorSettingsSpeedOne.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedTwo"].Value = mcp.textBoxActuatorSettingsSpeedTwo.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedThree"].Value = mcp.textBoxActuatorSettingsSpeedThree.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedOne"].Value = mcp.textBoxActuatorSettingsMicroSpeedOne.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedTwo"].Value = mcp.textBoxActuatorSettingsMicroSpeedTwo.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedThree"].Value = mcp.textBoxActuatorSettingsMicroSpeedThree.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationOne"].Value = mcp.textBoxActuatorSettingsAccelerationOne.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationTwo"].Value = mcp.textBoxActuatorSettingsAccelerationTwo.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationThree"].Value = mcp.textBoxActuatorSettingsAccelerationThree.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationOne"].Value = mcp.textBoxActuatorSettingsDecelerationOne.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationTwo"].Value = mcp.textBoxActuatorSettingsDecelerationTwo.Text;
                configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationThree"].Value = mcp.textBoxActuatorSettingsDecelerationThree.Text;

                // Connection Settings Panel
                List<string> axisOrder = mcp.GetConnectionSettingsComboBoxSelectedIndex();
                configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceOneSelectedIndex"].Value = axisOrder[0];
                configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceTwoSelectedIndex"].Value = axisOrder[1];
                configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceThreeSelectedIndex"].Value = axisOrder[2];

                // Move relatively Panel
                // Multiple devices
                List<string> moveRelativelyIndexOrder = mcp.GetMoveRelativelyMultipleDevicesComboBoxSelectedIndex();
                configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceOneSelectedIndex"].Value = moveRelativelyIndexOrder[0];
                configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceTwoSelectedIndex"].Value = moveRelativelyIndexOrder[1];
                configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceThreeSelectedIndex"].Value = moveRelativelyIndexOrder[2];

                configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsOne"].Value = mcp.textBoxMoveRelativelyToStepsOne.Text;
                configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsOne"].Value = mcp.textBoxMoveRelativelyToMicroStepsOne.Text;

                configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsTwo"].Value = mcp.textBoxMoveRelativelyToStepsTwo.Text;
                configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsTwo"].Value = mcp.textBoxMoveRelativelyToMicroStepsTwo.Text;

                configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsThree"].Value = mcp.textBoxMoveRelativelyToStepsThree.Text;
                configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsThree"].Value = mcp.textBoxMoveRelativelyToMicroStepsThree.Text;

                // One device
                configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsOneDevice"].Value = mcp.textBoxMoveToPositionStepsOneDevice.Text;
                configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsOneDevice"].Value = mcp.textBoxMoveToPositionMicroStepsOneDevice.Text;

                configuration.AppSettings.Settings["textBoxShiftToStepsOneDevice"].Value = mcp.textBoxShiftToStepsOneDevice.Text;
                configuration.AppSettings.Settings["textBoxShiftToMicroStepsOneDevice"].Value = mcp.textBoxShiftToMicroStepsOneDevice.Text;

                configuration.AppSettings.Settings["textBoxCycleBeweenFromStepsOneDevice"].Value = mcp.textBoxCycleBeweenFromStepsOneDevice.Text;
                configuration.AppSettings.Settings["textBoxCycleBeweenToStepsOneDevice"].Value = mcp.textBoxCycleBeweenToStepsOneDevice.Text;

                // Software limits for actuator
                configuration.AppSettings.Settings["minEdgePositionStepsAllDevices"].Value = "-11000";
                configuration.AppSettings.Settings["maxEdgePositionStepsAllDevices"].Value = "11000";
                configuration.AppSettings.Settings["homePositionStepsAllDevices"].Value = "0";

                /*configuration.AppSettings.Settings["minEdgePositionStepsDeviceTwo"].Value = "-11000";
                configuration.AppSettings.Settings["maxEdgePositionStepsDeviceTwo"].Value = "11000";
                configuration.AppSettings.Settings["homePositionStepsDeviceTwo"].Value = "0";

                configuration.AppSettings.Settings["minEdgePositionStepsDeviceThree"].Value = "-11000";
                configuration.AppSettings.Settings["maxEdgePositionStepsDeviceThree"].Value = "11000";
                configuration.AppSettings.Settings["homePositionStepsDeviceThree"].Value = "0";*/

                // Save the configuration to App.config
                configuration.Save(ConfigurationSaveMode.Minimal);
            }
            catch { }

        }

        public void LoadAllFromConfiguration()
        {
            try
            {
                // Software limits for actuators
                string minEdge = configuration.AppSettings.Settings["minEdgePositionStepsAllDevices"].Value;
                string maxEdge = configuration.AppSettings.Settings["maxEdgePositionStepsAllDevices"].Value;
                string homePos = configuration.AppSettings.Settings["homePositionStepsAllDevices"].Value;

                mcp.Apsl = new ActuatorPositionSoftwareLimits(int.Parse(minEdge), int.Parse(maxEdge), int.Parse(homePos));

                // Actuator Settings panel
                mcp.textBoxActuatorSettingsSpeedOne.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedOne"].Value;
                mcp.textBoxActuatorSettingsSpeedTwo.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedTwo"].Value;
                mcp.textBoxActuatorSettingsSpeedThree.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsSpeedThree"].Value;
                mcp.textBoxActuatorSettingsMicroSpeedOne.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedOne"].Value;
                mcp.textBoxActuatorSettingsMicroSpeedTwo.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedTwo"].Value;
                mcp.textBoxActuatorSettingsMicroSpeedThree.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsMicroSpeedThree"].Value;
                mcp.textBoxActuatorSettingsAccelerationOne.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationOne"].Value;
                mcp.textBoxActuatorSettingsAccelerationTwo.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationTwo"].Value;
                mcp.textBoxActuatorSettingsAccelerationThree.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsAccelerationThree"].Value;
                mcp.textBoxActuatorSettingsDecelerationOne.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationOne"].Value;
                mcp.textBoxActuatorSettingsDecelerationTwo.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationTwo"].Value;
                mcp.textBoxActuatorSettingsDecelerationThree.Text = configuration.AppSettings.Settings["textBoxActuatorSettingsDecelerationThree"].Value;

                // Connection Settings Panel
                mcp.comboBoxConnectionSettingsDeviceOne.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceOneSelectedIndex"].Value);
                mcp.comboBoxConnectionSettingsDeviceTwo.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceTwoSelectedIndex"].Value);
                mcp.comboBoxConnectionSettingsDeviceThree.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxConnectionSettingsDeviceThreeSelectedIndex"].Value);

                // Move relatively Panel
                // Multiple devices
                mcp.comboBoxMoveRelativelyDeviceOne.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceOneSelectedIndex"].Value);
                mcp.comboBoxMoveRelativelyDeviceTwo.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceTwoSelectedIndex"].Value);
                mcp.comboBoxMoveRelativelyDeviceThree.SelectedIndex = int.Parse(configuration.AppSettings.Settings["comboBoxMoveRelativelyDeviceThreeSelectedIndex"].Value);

                mcp.textBoxMoveRelativelyToStepsOne.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsOne"].Value;
                mcp.textBoxMoveRelativelyToMicroStepsOne.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsOne"].Value;

                mcp.textBoxMoveRelativelyToStepsTwo.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsTwo"].Value;
                mcp.textBoxMoveRelativelyToMicroStepsTwo.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsTwo"].Value;

                mcp.textBoxMoveRelativelyToStepsThree.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsThree"].Value;
                mcp.textBoxMoveRelativelyToMicroStepsThree.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsThree"].Value;

                // One device
                mcp.textBoxMoveToPositionStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToStepsOneDevice"].Value;
                mcp.textBoxMoveToPositionMicroStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxMoveRelativelyToMicroStepsOneDevice"].Value;

                mcp.textBoxShiftToStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxShiftToStepsOneDevice"].Value;
                mcp.textBoxShiftToMicroStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxShiftToMicroStepsOneDevice"].Value;

                mcp.textBoxCycleBeweenFromStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxCycleBeweenFromStepsOneDevice"].Value;
                mcp.textBoxCycleBeweenToStepsOneDevice.Text = configuration.AppSettings.Settings["textBoxCycleBeweenToStepsOneDevice"].Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // to handle
            }
        }
    }
}
