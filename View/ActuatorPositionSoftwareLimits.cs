﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View
{
    public class ActuatorPositionSoftwareLimits
    {
        private int minEdgePositionStepsAllDevices;
        private int maxEdgePositionStepsAllDevices;
        private int homePositionStepsAllDevices;
        private int minPositionMicroStepsAllDevices;
        private int maxPositionMicroStepsAllDevices;

        public ActuatorPositionSoftwareLimits(int minEdgePositionStepsAllDevices, int maxEdgePositionStepsAllDevices, int homePositionStepsAllDevices)
        {
            this.MinEdgePositionStepsAllDevices = minEdgePositionStepsAllDevices;
            this.MaxEdgePositionStepsAllDevices = maxEdgePositionStepsAllDevices;
            this.HomePositionStepsAllDevices = homePositionStepsAllDevices;
            this.MinPositionMicroStepsAllDevices = -255;
            this.MaxPositionMicroStepsAllDevices = 255;
        }

        public int MinEdgePositionStepsAllDevices { get => minEdgePositionStepsAllDevices; set => minEdgePositionStepsAllDevices = value; }
        public int MaxEdgePositionStepsAllDevices { get => maxEdgePositionStepsAllDevices; set => maxEdgePositionStepsAllDevices = value; }
        public int HomePositionStepsAllDevices { get => homePositionStepsAllDevices; set => homePositionStepsAllDevices = value; }
        public int MinPositionMicroStepsAllDevices { get => minPositionMicroStepsAllDevices; set => minPositionMicroStepsAllDevices = value; }
        public int MaxPositionMicroStepsAllDevices { get => maxPositionMicroStepsAllDevices; set => maxPositionMicroStepsAllDevices = value; }
    }
}
